package com.gkuharenko.task1.service.api;


import com.gkuharenko.task1.exception.ValidationFailedException;

public interface Validator<T> {

    /**
     * The method validates the received parameter.
     *
     * @param data - data
     * @throws ValidationFailedException
     */
    void validate(T data) throws ValidationFailedException;

}
