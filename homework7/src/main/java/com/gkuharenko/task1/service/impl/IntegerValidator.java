package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.exception.ValidationFailedException;
import com.gkuharenko.task1.service.api.Validator;

public class IntegerValidator implements Validator<Integer> {

    private static final int START_OF_INTERVAL = 1;
    private static final int END_OF_INTERVAL = 10;
    private static final String VALIDATION_EXCEPTION_MSG = "Integer numbers must belong to the interval [1,10].";
    private static final String VALIDATION_SUCCESS_MSG = "***Number validation completed success!***";

    /**
     * The method validates an integer.
     *
     * @param number - number
     * @throws ValidationFailedException - exception
     */
    @Override
    public void validate(Integer number) throws ValidationFailedException {
        if (number >= START_OF_INTERVAL && number <= END_OF_INTERVAL) {
            System.out.println(VALIDATION_SUCCESS_MSG);
        } else {
            throw new ValidationFailedException(VALIDATION_EXCEPTION_MSG);
        }
    }
}
