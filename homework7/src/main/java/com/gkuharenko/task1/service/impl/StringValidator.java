package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.exception.ValidationFailedException;
import com.gkuharenko.task1.service.api.Validator;

public class StringValidator implements Validator<String> {

    private static final String REGEX_FOR_VALIDATION = "^[A-Z][A-Za-z ].*$";
    private static final String VALIDATION_SUCCESS_MSG = "***String validation completed success!***";
    private static final String VALIDATION_EXCEPTION_MSG = "The line must begin with a capital letter.";

    /**
     * The method validates the string
     *
     * @param line - line
     * @throws ValidationFailedException - exception
     */
    @Override
    public void validate(String line) throws ValidationFailedException {
        if (line.matches(REGEX_FOR_VALIDATION)) {
            System.out.println(VALIDATION_SUCCESS_MSG);
        } else {
            throw new ValidationFailedException(VALIDATION_EXCEPTION_MSG);
        }
    }
}
