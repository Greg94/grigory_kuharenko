package com.gkuharenko.task1;

import com.gkuharenko.task1.exception.ValidationFailedException;
import com.gkuharenko.task1.util.ValidationSystem;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class TaskPresentation {

    private static final Logger LOG = Logger.getLogger(TaskPresentation.class);

    private static final String ACCEPT = "1";
    private static final String DECLINE = "0";
    private static final String VALIDATION_ERROR_MSG = "Validation error.";

    private Object data;

    /**
     * Method launches the application
     */
    public void startPresentation() {
        try (Scanner scanner = new Scanner(System.in)) {
            doValidation(scanner);
        }
    }

    /**
     * Menu display and validation of received data.
     *
     * @param scanner - scanner
     */
    private void doValidation(Scanner scanner) {
        while (true) {
            showWelcomeMsg();
            String input = scanner.next();
            if (input.equals(ACCEPT)) {
                System.out.println("###Select data to validate: integer or line###");
                checkInput(scanner);
                try {
                    ValidationSystem.validate(data);
                } catch (ValidationFailedException e) {
                    LOG.error(VALIDATION_ERROR_MSG, e);
                }
            } else if (input.equals(DECLINE)) {
                break;
            }
        }
    }

    /**
     * Method checks data entry.
     *
     * @param scanner - scanner
     */
    private void checkInput(Scanner scanner) {
        if (scanner.hasNextInt()) {
            data = scanner.nextInt();
        } else if (scanner.hasNextLine()) {
            scanner.nextLine();
            data = scanner.nextLine();
        }
    }

    /**
     * Displays a message.
     */
    private void showWelcomeMsg() {
        System.out.println("\n" + "########MENU########");
        System.out.println("Check data?");
        System.out.println("1 - Yes.");
        System.out.println("0 - No.");
    }

}
