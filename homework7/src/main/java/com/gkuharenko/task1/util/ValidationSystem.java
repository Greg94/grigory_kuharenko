package com.gkuharenko.task1.util;

import com.gkuharenko.task1.exception.ValidationFailedException;
import com.gkuharenko.task1.service.api.Validator;
import com.gkuharenko.task1.service.impl.IntegerValidator;
import com.gkuharenko.task1.service.impl.StringValidator;

import java.util.HashMap;
import java.util.function.Supplier;

public final class ValidationSystem {

    private static final String DATA_IS_INTEGER = "Integer";
    private static final String DATA_IS_STRING = "String";

    private ValidationSystem() {

    }

    public static HashMap<String, Supplier<Validator>> validatorMap = new HashMap<String, Supplier<Validator>>() {
        {
            put(DATA_IS_INTEGER, IntegerValidator::new);
            put(DATA_IS_STRING, StringValidator::new);
        }
    };

    /**
     * The method checks the received parameter and calls the desired validator.
     *
     * @param data - data
     * @throws ValidationFailedException - exception
     */
    public static void validate(Object data) throws ValidationFailedException {
        String typeName = data.getClass().getSimpleName();
        Validator validator = validatorMap.get(typeName).get();
        validator.validate(data);
    }

}