package com.gkuharenko.task1;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TextReaderTest {

    @Test
    public void testTextParse() {

        String expected = "A: \ta 1\nH: \thillside 1\nL: \tlooking 1\nU: \tup 1\nW: \twhen 1\n";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(outputStream);
        System.setOut(ps);

        TextReader.textParse("A hillside, when, looking up.");

        Assert.assertEquals(expected, outputStream.toString());

    }
}