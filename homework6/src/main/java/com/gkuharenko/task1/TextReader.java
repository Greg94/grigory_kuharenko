package com.gkuharenko.task1;

import java.util.*;
import java.util.stream.Collectors;

public final class TextReader {

    private static final String REGEX_REPLACE = "[^a-zA-Z ]";
    private static final String SPACE = " ";
    private static final String COLON = ": ";
    private static final char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    private TextReader() {
    }

    /**
     * The utility method takes the text and breaks it into separate words
     *
     * @param text - text
     */
    public static void textParse(String text) {
        for (char letter : alphabet) {
            String[] words = text.replaceAll(REGEX_REPLACE, SPACE).toLowerCase().split(SPACE);
            Set<String> findWords = Arrays.stream(words).filter(s -> s.startsWith(String.valueOf(letter))).collect(Collectors.toSet());
            if (!findWords.isEmpty()) {
                System.out.print(String.valueOf(letter).toUpperCase() + COLON);
            }
            for (String word : findWords) {
                int count = (int) Arrays.stream(words).filter(s -> s.equals(word)).count();
                System.out.print("\t" + word + SPACE + count + "\n");
            }
        }
    }
}