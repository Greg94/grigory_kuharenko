package com.gkuharenko.task1;

import java.util.Scanner;

public class Main {

    private static final String EXIT = "q";
    private static final String WELCOME_MSG = "Enter text for analysis: ";

    /**
     * main method that launches the application
     *
     * @param args - arguments
     */
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                System.out.println(WELCOME_MSG);
                String text = scanner.nextLine();
                if (text.equals(EXIT)) {
                    break;
                }
                TextReader.textParse(text);
            }
        }
    }
}

    /*
    INPUT TEXT EXAMPLE
    Once upon a time a Wolf was lapping at a spring on a hillside, when, looking up, what should he see but a Lamb just beginning to drink a little lower down.
     */
