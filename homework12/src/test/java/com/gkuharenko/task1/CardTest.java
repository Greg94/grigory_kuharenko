package com.gkuharenko.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CardTest {

    private Card card;

    @Before
    public void setUp() throws Exception {
        card = new Card();
    }

    @Test
    public void tesdAddMoney() {
        int expected = 550;
        card.addMoney(50);
        Assert.assertEquals(expected, card.getBalance());
    }

    @Test
    public void testWithdrawMoney() {
        int expected = 350;
        card.withdrawMoney(150);
        Assert.assertEquals(expected,card.getBalance());
    }
}