package com.gkuharenko.task1;


import static com.gkuharenko.task1.Card.MAX_BALANCE;
import static com.gkuharenko.task1.Card.MIN_BALANCE;

public class Husband implements Runnable {


    private final Card card;

    public Husband(Card card) {
        this.card = card;
    }

    /**
     * Method is called when creating a thread.
     * The method checks whether it can continue the operation with the card,
     * if so, it performs the operation
     */
    public void run() {
        while (card.getContinueOperationWithCard()) {
            int randomTime = 2000 + (int) (Math.random() * 4000); //random time 2-5 sec
            try {
                Thread.sleep(randomTime);
            } catch (InterruptedException e) {
                break;
            }
            if (card.getBalance() >= MAX_BALANCE) {
                card.setContinueOperationWithCard(Boolean.FALSE);
                System.out.println("Husband " + Thread.currentThread().getName() + " wins!");
                break;
            } else if (card.getBalance() < MAX_BALANCE && card.getBalance() > MIN_BALANCE) {
                makeAdd();
            }
        }
    }

    /**
     * The method adds a random amount of money from 5 to 10 dollars
     */
    private void makeAdd() {
        int randomMoney = 5 + (int) (Math.random() * 6); //random money 5-10$
        card.addMoney(randomMoney);
        System.out.printf("Husband " + Thread.currentThread().getName() + " put on the card: %s$, balance: %s$\n", randomMoney, card.getBalance());
    }

}


