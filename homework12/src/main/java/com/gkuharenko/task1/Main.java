package com.gkuharenko.task1;

public class Main {
    public static void main(String[] args) {
        Card card = new Card();
        Atm atm = new Atm(card);
        atm.startPresentation();
    }
}
