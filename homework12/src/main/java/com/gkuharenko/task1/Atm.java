package com.gkuharenko.task1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Atm {

    private Husband husband;
    private Wife wife;

    public Atm(Card card) {
        this.husband = new Husband(card);
        this.wife = new Wife(card);
    }

    /**
     * Runs the entire application passing random arguments
     */
    public void startPresentation() {
        int randomCountConsumers = 3 + (int) (Math.random() * 3); //random count 3-5
        int randomCountProducers = 3 + (int) (Math.random() * 3); //random count 3-5
        System.out.println("Consumers: " + randomCountConsumers);
        System.out.println("Producers: " + randomCountProducers);
        setProducers(randomCountProducers);
        setConsumers(randomCountConsumers);
    }

    /**
     * Creates an executorservice and adds the transferred number of producers flows to it
     *
     * @param countProducers - count producers
     */
    private void setProducers(int countProducers) {
        ExecutorService producer = Executors.newFixedThreadPool(countProducers);
        for (int j = 0; j < countProducers; j++) {
            producer.execute(husband);
        }
        producer.shutdown();
    }

    /**
     * Creates an executorservice and adds the transferred number of consumers flows to it
     *
     * @param countConsumers - count producers
     */
    private void setConsumers(int countConsumers) {
        ExecutorService consumer = Executors.newFixedThreadPool(countConsumers);
        for (int j = 0; j < countConsumers; j++) {
            consumer.execute(wife);
        }
        consumer.shutdown();
    }

}








