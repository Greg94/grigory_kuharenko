package com.gkuharenko.task2;

import com.gkuharenko.task2.model.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * In this class, I simulate the work of a service randomly generate construction crews and run the application
 */
public class TaskPresentation {

    private static final List<String> EMPLOYEE_NAME = Arrays.asList("Ravshan", "Djumshut", "Petya", "Vasya",
            "Shura", "Gena", "Artur", "Serega", "Volodya", "Ashot", "Djigurda");
    private static final List<BigDecimal> PRICE_FOR_EMPLOYEE = Arrays.asList(BigDecimal.valueOf(25.23),
            BigDecimal.valueOf(43.11), BigDecimal.valueOf(32.42), BigDecimal.valueOf(74.21),
            BigDecimal.valueOf(12.00), BigDecimal.valueOf(32.56), BigDecimal.valueOf(85.99),
            BigDecimal.valueOf(55.30), BigDecimal.valueOf(41.10));
    private static final List<Speciality> SPECIALITY_LIST = new ArrayList<>(Arrays.asList(Speciality.values()));

    public static void main(String[] args) {
        TaskPresentation taskPresentation = new TaskPresentation();

        Contract contract = new Contract();
        contract.addRequiredSkills(1, Speciality.BRIGADIER);
        contract.addRequiredSkills(2, Speciality.WELDER);
        contract.addRequiredSkills(1, Speciality.ADJUSTER);
        contract.addRequiredSkills(2,Speciality.ELECTRIC);

        TenderService tenderService = new TenderService();
        taskPresentation.addTeamsToTender(tenderService, 1000);
        tenderService.findWinner(contract);
    }

    private void addTeamsToTender(TenderService tenderService, int countTeam) {
        for (ConstructionTeam team : getConstructionTeamList(countTeam)) {
            tenderService.addConstructionTeam(team);
        }
    }

    private List<ConstructionTeam> getConstructionTeamList(int countTeam) {
        List<ConstructionTeam> constructionTeamList = new ArrayList<>();
        for (int i = 0; i < countTeam; i++) {
            int randomCountEmployees = (int) (Math.random() * (3 + 1) + 3); //construction team can consist of 3 to 6 employees
            constructionTeamList.add(initConstructionTeam("Team" + i, randomCountEmployees));
        }
        return constructionTeamList;
    }

    private ConstructionTeam initConstructionTeam(String teamName, int countEmployees) {
        ConstructionTeam team = new ConstructionTeam(teamName);
        List<String> randomEmployee = getRandomEmployee(countEmployees);
        randomEmployee.stream().map(Employee::new)
                .map(this::addSpecialityForEmployee)
                .map(this::addPriceForEmployee)
                .forEach(team::addEmployee);
        return team;
    }

    private List<String> getRandomEmployee(int countEmployees) {
        Collections.shuffle(EMPLOYEE_NAME);
        List<String> returnList = new ArrayList<>(EMPLOYEE_NAME);
        if (returnList.size() > countEmployees) {
            returnList.subList(countEmployees, returnList.size()).clear();
        }
        return returnList;
    }

    private Employee addSpecialityForEmployee(Employee employee) {
        int randomSpeciality = new Random().nextInt(SPECIALITY_LIST.size());
        employee.addSpeciality(SPECIALITY_LIST.get(randomSpeciality));
        return employee;
    }

    private Employee addPriceForEmployee(Employee employee) {
        int randomPrice = new Random().nextInt(PRICE_FOR_EMPLOYEE.size());
        employee.setPrice(PRICE_FOR_EMPLOYEE.get(randomPrice));
        return employee;
    }

}
