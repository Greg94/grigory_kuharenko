package com.gkuharenko.task2.model;

import com.gkuharenko.task2.exception.ConstructionTeamSearchException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TenderService {

    private static final String SEARCH_ERROR_MSG = "No suitable teams.";

    private List<ConstructionTeam> allTeams;
    private List<ConstructionTeam> suitableTeams;

    public TenderService() {
        this.allTeams = new ArrayList<>();
        this.suitableTeams = new ArrayList<>();
    }

    /**
     * The method adds all the teams participating in the tender
     *
     * @param team - team
     */
    public void addConstructionTeam(ConstructionTeam team) {
        allTeams.add(team);
    }

    /**
     * The method determines the winner of the tender.
     *
     * @param contract - contract
     */
    public ConstructionTeam findWinner(Contract contract) {
        choiceTeam(contract);
        if (!suitableTeams.isEmpty()) {
            showSuitableTeams(suitableTeams);
        }
        try {
            System.out.println("Сonstruction team winner: " + compareTeamsForPrice().getTeamName());
            return compareTeamsForPrice();
        } catch (ConstructionTeamSearchException e) {
            System.out.println("The construction project is closed. There will be no national library. =(");
            return null;
        }
    }

    /**
     * Method shows suitable teams for the contract.
     *
     * @param suitableTeams - suitable teams.
     */
    private void showSuitableTeams(List<ConstructionTeam> suitableTeams) {
        System.out.println("Suitable construction teams:");
        for (ConstructionTeam team : suitableTeams) {
            team.showTeam();
        }
    }

    /**
     * The method selects from all teams suitable for the contract.
     *
     * @param contract - contract.
     */
    private void choiceTeam(Contract contract) {
        List<Speciality> requiredSpecialties = contract.getRequiredSkills();
        requiredSpecialties.sort(Enum::compareTo);
        for (ConstructionTeam team : allTeams) {
            List<Speciality> specialityConstructionTeam = team.getSpecialityList();
            team.getSpecialityList().sort(Enum::compareTo);
            if (specialityConstructionTeam.equals(requiredSpecialties)) {
                suitableTeams.add(team);
            }
        }
    }

    /**
     * Method compares suitable teams for the price.
     *
     * @return - team with the lowest cost.
     * @throws ConstructionTeamSearchException - if no suitable teams are found.
     */
    private ConstructionTeam compareTeamsForPrice() throws ConstructionTeamSearchException {
        return suitableTeams.stream().min(Comparator.comparing(ConstructionTeam::getPrice)).orElseThrow(() ->
                new ConstructionTeamSearchException(SEARCH_ERROR_MSG));
    }

}
