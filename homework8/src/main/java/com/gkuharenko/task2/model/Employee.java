package com.gkuharenko.task2.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.gkuharenko.task2.model.ConstructionTeam.SCALE;
import static com.gkuharenko.task2.model.ConstructionTeam.START_PRICE;

public class Employee {
    private String name;
    private List<Speciality> specialityList;
    private BigDecimal price;

    public Employee(String name) {
        this.name = name;
        this.specialityList = new ArrayList<>();
        this.price = new BigDecimal(START_PRICE);
    }

    /**
     * Method return speciality list.
     *
     * @return speciality list.
     */
    public List<Speciality> getSpecialityList() {
        return specialityList;
    }

    /**
     * Method return price.
     *
     * @return - price
     */
    public BigDecimal getPrice() {
        return price.setScale(SCALE, BigDecimal.ROUND_UP);
    }

    /**
     * Method set price.
     *
     * @param price - price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Method add speciality.
     *
     * @param speciality - speciality.
     */
    public void addSpeciality(Speciality speciality) {
        specialityList.add(speciality);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", specialityList=" + specialityList +
                ", price=" + price +
                '}';
    }
}
