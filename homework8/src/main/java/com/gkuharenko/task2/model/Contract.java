package com.gkuharenko.task2.model;


import java.util.ArrayList;
import java.util.List;

public class Contract {

    private List<Speciality> requiredSkills;

    public Contract() {
        this.requiredSkills = new ArrayList<>();
    }

    public List<Speciality> getRequiredSkills() {
        return requiredSkills;
    }

    /**
     * The method adds the required specialties to the contract and the number of employees based on the number of specialties.
     *
     * @param countEmployee - number of people with a particular specialty.
     * @param speciality    - speciality.
     */
    public void addRequiredSkills(int countEmployee, Speciality speciality) {
        for (int i = 0; i < countEmployee; i++) {
            requiredSkills.add(speciality);
        }
    }

}
