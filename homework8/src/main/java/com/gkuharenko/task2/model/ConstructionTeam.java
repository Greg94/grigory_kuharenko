package com.gkuharenko.task2.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ConstructionTeam {
    public static final int START_PRICE = 0;
    public static final int SCALE = 2;

    private String teamName;
    private List<Employee> employeeList;
    private List<Speciality> specialityList;
    private BigDecimal price;


    public ConstructionTeam(String teamName) {
        this.teamName = teamName;
        this.employeeList = new ArrayList<>();
        this.specialityList = new ArrayList<>();
        this.price = new BigDecimal(START_PRICE);
    }

    /**
     * Method return team name.
     *
     * @return - team name.
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * Method return list specialities.
     *
     * @return list speciality.
     */
    public List<Speciality> getSpecialityList() {
        return specialityList;
    }

    /**
     * Method return list employees.
     * @return - list employees
     */
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    /**
     * Method return price.
     *
     * @return - price.
     */
    public BigDecimal getPrice() {
        return price.setScale(SCALE, BigDecimal.ROUND_UP);
    }

    /**
     * Method adds employee, price and speciality to the list.
     * If the employee has several specialties, then one is randomly selected
     *
     * @param employee - employee.
     */
    public void addEmployee(Employee employee) {
        employeeList.add(employee);
        this.price = price.add(employee.getPrice());
        int randomSpeciality = new Random().nextInt(employee.getSpecialityList().size());
        specialityList.add(employee.getSpecialityList().get(randomSpeciality));
    }

    /**
     * The method shows the state of the construction team.
     */
    public void showTeam() {
        System.out.printf("Team name: %s, price: %s. Consist of: %s. \n", teamName, price, employeeList);
    }

    @Override
    public String toString() {
        return "ConstructionTeam{" +
                "employeeList=" + employeeList +
                ", specialityList=" + specialityList +
                '}';
    }
}
