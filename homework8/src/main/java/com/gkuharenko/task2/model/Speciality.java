package com.gkuharenko.task2.model;

public enum Speciality {

    BRIGADIER,
    BRICKLAYER,
    WELDER,
    HANDYMAN,
    ADJUSTER,
    ELECTRIC

}
