package com.gkuharenko.task2.exception;

public class ConstructionTeamSearchException extends Exception {
    public ConstructionTeamSearchException(String message) {
        super(message);
    }
}
