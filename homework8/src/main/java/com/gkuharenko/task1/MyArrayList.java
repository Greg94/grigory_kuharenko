package com.gkuharenko.task1;

import java.util.Arrays;

public class MyArrayList<E> {

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final String OVERFLOW_ERROR = "The list is full.";
    private static final String INDENT = " ";

    private static final Object[] EMPTY_ELEMENTDATA = {};
    private static final int DEFAULT_CAPASITY = 10;
    private int size;
    private Object[] data;
    private int capasity;

    public MyArrayList(int capasity) {
        if (capasity > ZERO) {
            this.data = new Object[capasity];
            this.capasity = capasity;
        } else {
            throw new IllegalArgumentException("Illegal capacity: " + capasity);
        }
    }

    public MyArrayList() {
        this.data = new Object[DEFAULT_CAPASITY];
        this.capasity = DEFAULT_CAPASITY;
    }

    /**
     * Method checks if index is suitable.
     *
     * @param index - index.
     * @throws IndexOutOfBoundsException - index goes beyond
     */
    private void checkIndex(int index) {
        if (index < ZERO || index >= capasity)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }

    /**
     * The method returns the number of objects in the list.
     *
     * @return - size list.
     */
    public int size() {
        return size;
    }

    /**
     * Method checks if there are objects inside the sheet. If there is something true, if not then false.
     *
     * @return true or false.
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * The method accepts an object, checks if there is enough space in the array,
     * if it sufficiently increases size by one adding an object.
     *
     * @param e - object to be added.
     * @return - true if ok, else false.
     * @throws ListOverflowException - capacity is full.
     */
    public boolean add(E e) {
        ensureCapasity();
        data[size++] = e;
        return true;
    }

    /**
     * Checks index and returns object by array index.
     *
     * @param index - index.
     * @return - object by index.
     * @throws IndexOutOfBoundsException - index goes beyond.
     */
    @SuppressWarnings("unchecked")
    public E get(int index) {
        checkIndex(index);
        return (E) data[index];
    }

    /**
     * Checks the index and replaces the object at the passed index with the passed object.
     *
     * @param index   - index.
     * @param element - new element.
     * @return - true if ok, else false.
     * @throws IndexOutOfBoundsException - index goes beyond.
     */
    public boolean set(int index, E element) {
        checkIndex(index);
        data[index] = element;
        return true;
    }

    /**
     * The method checks the index and checks the capacity,
     * and then using the System.arraycopy method moves elements one cell to the right of the transmitted index.
     *
     * @param index - index.
     * @param elem  - new element.
     * @throws IndexOutOfBoundsException - index goes beyond.
     * @throws ListOverflowException     - capacity is full.
     */
    public void insert(int index, E elem) {
        checkIndex(index);
        ensureCapasity();
        System.arraycopy(data, index, data, index + ONE, size - index);
        data[index] = elem;
        size++;
    }

    /**
     * The method searches for an element in the array by index.
     * If finds, then removes and shifts the elements one cell to the left, returning the true, else false.
     *
     * @param index - index.
     * @return - if ok true, else false.
     * @throws IndexOutOfBoundsException - index goes beyond.
     */
    public boolean removeAt(int index) {
        checkIndex(index);
        int numMoved = size - index - ONE;
        if (numMoved > ZERO) {
            System.arraycopy(data, index + ONE, data, index, numMoved);
        }
        data[--size] = null;
        return true;
    }

    /**
     * The method searches for the passed object in the array.
     * If it finds, then removes it and returns true, otherwise false.
     *
     * @param elem - search element.
     * @return - if ok true, else false.
     */
    public boolean remove(Object elem) {
        if (elem == null) {
            for (int index = ZERO; index < size; index++)
                if (data[index] == null) {
                    fastRemove(index);
                    return true;
                }
        } else {
            for (int index = ZERO; index < size; index++)
                if (elem.equals(data[index])) {
                    fastRemove(index);
                    return true;
                }
        }
        return false;
    }

    /**
     * A private method that deletes by index and skips index checking.
     *
     * @param index - index.
     */
    private void fastRemove(int index) {
        int numMoved = size - index - ONE;
        if (numMoved > ZERO) {
            System.arraycopy(data, index + ONE, data, index, numMoved);
        }
        data[--size] = null;
    }

    /**
     * The method takes an element as input and if it finds it, then returns its index, else returns -1.
     *
     * @param o - search element.
     * @return - index if ok, else -1.
     */
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = ZERO; i < size; i++)
                if (data[i] == null)
                    return i;
        } else {
            for (int i = ZERO; i < size; i++)
                if (o.equals(data[i]))
                    return i;
        }
        return -1;
    }

    /**
     * Method clears an array.
     */
    public void clear() {
        for (int i = ZERO; i < size; i++) {
            data[i] = null;
        }
        size = ZERO;
    }

    /**
     * Overridden toString method.
     *
     * @return - string elements.
     */
    @Override
    public String toString() {
        StringBuilder mas = new StringBuilder();
        for (Object a : data) {
            mas.append(a).append(INDENT);
        }
        return mas.toString();
    }

    /**
     * The method cuts off the size of the container, where there are no elements.
     */
    public void trimToSize() {
        if (size < data.length) {
            data = (size == ZERO)
                    ? EMPTY_ELEMENTDATA
                    : Arrays.copyOf(data, size);
        }
    }

    /**
     * The private method checks to see if the array is full. If filled, then throws ListOverflowException.
     *
     * @throws ListOverflowException - capacity is full.
     */
    private void ensureCapasity() {
        if (this.size == data.length) {
            throw new ListOverflowException(OVERFLOW_ERROR);
        }
    }

    /**
     * Method returns an array of elements.
     *
     * @return - array.
     */
    public Object[] toArray() {
        return Arrays.copyOf(data, size);
    }

}
