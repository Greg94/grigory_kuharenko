package com.gkuharenko.task1;

public class ListOverflowException extends RuntimeException {
    public ListOverflowException(String message) {
        super(message);
    }
}
