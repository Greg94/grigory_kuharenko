package com.gkuharenko.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class MyArrayListTest {
    private MyArrayList<String> list;

    @Before
    public void setUp() throws Exception {
        list = new MyArrayList<>(3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createConstuctorWithZero(){
        list = new MyArrayList<>(0);
    }

    @Test
    public void testSize() {
        list.add("1");
        list.add("2");
        int expected = 2;
        Assert.assertEquals(expected, list.size());
    }

    @Test
    public void testSizeIfSizeZero() {
        int expected = 0;
        Assert.assertEquals(expected, list.size());
    }

    @Test
    public void testIsEmpty() {
        boolean expected = true;
        Assert.assertEquals(expected, list.isEmpty());
    }

    @Test

    public void testIsNotEmpty() {
        list.add("test");
        boolean expected = false;
        Assert.assertEquals(expected, list.isEmpty());
    }

    @Test
    public void testAdd() {
        boolean expected = true;
        Assert.assertEquals(expected, list.add("test"));
    }

    @Test(expected = ListOverflowException.class)
    public void testAddFails() {
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("exception");
    }

    @Test
    public void testGet() {
        list.add("0");
        list.add("1");
        String expected = "1";
        Assert.assertEquals(expected, list.get(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetFails() {
        list.get(3);
    }


    @Test
    public void testSet() {
        list.add("0");
        list.add("1");
        list.add("2");
        boolean expected = true;
        Assert.assertEquals(expected, list.set(1, "set"));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetFails() {
        list.add("0");
        list.add("1");
        list.add("2");
        list.set(4, "set");
    }

    @Test
    public void testInsert() {
        list.add("0");
        list.add("1");
        list.insert(1, "insert");
        String[] expected = {"0", "insert", "1"};
        Assert.assertArrayEquals(expected, list.toArray());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testInsertFails() {
        list.add("0");
        list.add("1");
        list.insert(3, "insert");
    }

    @Test(expected = ListOverflowException.class)
    public void testInsertFails2() {
        list.add("0");
        list.add("1");
        list.add("2");
        list.insert(1, "insert");
    }


    @Test
    public void testRemoveAt() {
        list.add("0");
        list.add("1");
        list.add("2");
        boolean expected = true;
        Assert.assertEquals(expected, list.removeAt(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveAtFail() {
        list.removeAt(4);
    }


    @Test
    public void testRemove() {
        list.add("0");
        list.add("1");
        list.add("2");
        boolean expected = true;
        Assert.assertEquals(expected, list.remove("2"));
    }

    @Test
    public void testRemoveNoObject() {
        list.add("0");
        list.add("1");
        list.add("2");
        boolean expected = false;
        Assert.assertEquals(expected, list.remove("3"));
    }

    @Test
    public void testIndexOf() {
        list.add("1");
        list.add("2");
        list.add("3");
        int expected = 2;
        Assert.assertEquals(expected, list.indexOf("3"));
    }

    @Test
    public void testIndexOfNoObject() {
        list.add("1");
        list.add("2");
        list.add("3");
        int expected = -1;
        Assert.assertEquals(expected, list.indexOf("4"));
    }

    @Test
    public void testClear() {
        list.add("0");
        list.add("1");
        list.add("2");
        String[] expected = {};
        list.clear();
        Assert.assertArrayEquals(expected, list.toArray());
    }

    @Test
    public void testToArray() {
        list.add("0");
        list.add("1");
        list.add("2");
        String[] expectedArray = {"0", "1", "2"};
        Assert.assertArrayEquals(expectedArray, list.toArray());
    }
}