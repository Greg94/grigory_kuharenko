package com.gkuharenko.task2.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConstructionTeamTest {

    private ConstructionTeam team;

    private Employee employee1;
    private Employee employee2;
    private Employee employee3;

    @Before
    public void setUp() throws Exception {
        team = new ConstructionTeam("team1");
        employee1 = new Employee("employee1");
        employee1.setPrice(BigDecimal.valueOf(11));
        employee1.addSpeciality(Speciality.BRIGADIER);

        employee2 = new Employee("employee2");
        employee2.setPrice(BigDecimal.valueOf(12));
        employee2.addSpeciality(Speciality.WELDER);

        employee3 = new Employee("employee3");
        employee3.setPrice(BigDecimal.valueOf(13));
        employee3.addSpeciality(Speciality.HANDYMAN);

    }

    @Test
    public void testAddEmployee() {
        List<Employee> expectedList = Arrays.asList(employee1, employee2, employee3);
        team.addEmployee(employee1);
        team.addEmployee(employee2);
        team.addEmployee(employee3);
        Assert.assertArrayEquals(expectedList.toArray(), team.getEmployeeList().toArray());
    }

    @Test
    public void testAddEmployeeCheckPrice() {
        BigDecimal expected = employee1.getPrice().add(employee2.getPrice()).add(employee3.getPrice());
        team.addEmployee(employee1);
        team.addEmployee(employee2);
        team.addEmployee(employee3);
        Assert.assertEquals(expected, team.getPrice());
    }

    @Test
    public void testAddEmployeeCheckSpecialities() {
        List <Speciality> expected = new ArrayList<>();
        expected.add(Speciality.BRIGADIER);
        expected.add(Speciality.WELDER);
        expected.add(Speciality.HANDYMAN);

        team.addEmployee(employee1);
        team.addEmployee(employee2);
        team.addEmployee(employee3);
        Assert.assertArrayEquals(expected.toArray(), team.getSpecialityList().toArray());
    }

}