package com.gkuharenko.task2.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ContractTest {

    private Contract contract;

    @Before
    public void setUp() throws Exception {
        contract = new Contract();
    }

    @Test
    public void testAddRequiredSkills() {
        List<Speciality> expected = new ArrayList<>();
        expected.add(Speciality.BRIGADIER);
        expected.add(Speciality.HANDYMAN);
        expected.add(Speciality.ELECTRIC);
        expected.add(Speciality.ELECTRIC);

        contract.addRequiredSkills(1, Speciality.BRIGADIER);
        contract.addRequiredSkills(1, Speciality.HANDYMAN);
        contract.addRequiredSkills(2, Speciality.ELECTRIC);
        Assert.assertArrayEquals(expected.toArray(), contract.getRequiredSkills().toArray());
    }
}