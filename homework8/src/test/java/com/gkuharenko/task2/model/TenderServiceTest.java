package com.gkuharenko.task2.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class TenderServiceTest {

    private TenderService tenderService;

    private Contract contract;

    private ConstructionTeam team1;
    private ConstructionTeam team2;

    private Employee employee1;
    private Employee employee2;
    private Employee employee3;
    private Employee employee4;
    private Employee employee5;
    private Employee employee6;

    @Before
    public void setUp() throws Exception {
        tenderService = new TenderService();

        contract = new Contract();

        team1 = new ConstructionTeam("team1");
        team2 = new ConstructionTeam("team2");

        employee1 = new Employee("employee1");
        employee1.setPrice(BigDecimal.valueOf(11));
        employee1.addSpeciality(Speciality.BRIGADIER);

        employee2 = new Employee("employee2");
        employee2.setPrice(BigDecimal.valueOf(12));
        employee2.addSpeciality(Speciality.WELDER);

        employee3 = new Employee("employee3");
        employee3.setPrice(BigDecimal.valueOf(13));
        employee3.addSpeciality(Speciality.HANDYMAN);

        employee4 = new Employee("employee4");
        employee4.setPrice(BigDecimal.valueOf(14));
        employee4.addSpeciality(Speciality.ELECTRIC);

        employee5 = new Employee("employee5");
        employee5.setPrice(BigDecimal.valueOf(15));
        employee5.addSpeciality(Speciality.ADJUSTER);

        employee6 = new Employee("employee6");
        employee6.setPrice(BigDecimal.valueOf(20));
        employee6.addSpeciality(Speciality.BRIGADIER);
    }

    @Test
    public void testFindWinner() {
        contract.addRequiredSkills(1, Speciality.BRIGADIER);
        contract.addRequiredSkills(1, Speciality.HANDYMAN);
        contract.addRequiredSkills(1, Speciality.ELECTRIC);

        team1.addEmployee(employee1);
        team1.addEmployee(employee4);
        team1.addEmployee(employee3);

        team2.addEmployee(employee6);
        team2.addEmployee(employee4);
        team2.addEmployee(employee3);

        tenderService.addConstructionTeam(team1);
        tenderService.addConstructionTeam(team2);

        ConstructionTeam expected = team1;

        Assert.assertEquals(expected, tenderService.findWinner(contract));

    }

    @Test
    public void testFindWinnerResultNull() {
        contract.addRequiredSkills(1, Speciality.BRIGADIER);
        contract.addRequiredSkills(1, Speciality.HANDYMAN);
        contract.addRequiredSkills(1, Speciality.ELECTRIC);

        team1.addEmployee(employee1);
        team1.addEmployee(employee4);
        team1.addEmployee(employee1);

        team2.addEmployee(employee2);
        team2.addEmployee(employee4);
        team2.addEmployee(employee5);

        tenderService.addConstructionTeam(team1);
        tenderService.addConstructionTeam(team2);

        Assert.assertNull(tenderService.findWinner(contract));

    }
}