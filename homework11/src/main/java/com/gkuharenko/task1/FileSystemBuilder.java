package com.gkuharenko.task1;


public class FileSystemBuilder {

    private static final String SEPARATOR = "/";
    private static final String REGEX_FILE = "^[\\w,\\s-]+\\.[A-Za-z]{3}$";
    private static final int ROOT_INDEX = 0;

    public static Tree<String> tree = new Tree<>();

    /**
     * Works with a string converting it to a file system hierarchy.
     *
     * @param path - path
     */
    public static void buildFileSystem(final String path) {
        String[] leafs = path.split(SEPARATOR);
        Tree.Branch<String> parent = null;
        for (String leaf : leafs) {
            if (parent == null) {
                if (tree.root == null) {
                    tree.root = new Tree.Branch<>(leaf, null);
                } else if (!tree.root.data.equals(leafs[ROOT_INDEX])) {
                    break;
                }
                parent = tree.root;
            } else if (parent.data.matches(REGEX_FILE)) {
                break;
            } else {
                Tree.Branch<String> found = null;
                for (Tree.Branch<String> child : parent.children) {
                    if (child.data.equals(leaf)) {
                        found = child;
                        break;
                    }
                }
                if (found == null) {
                    parent.children.add(found = new Tree.Branch<>(leaf, parent));
                }
                if (leaf.matches(REGEX_FILE)) break;
                parent = found;
            }
        }
    }

}
