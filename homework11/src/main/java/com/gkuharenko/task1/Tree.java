package com.gkuharenko.task1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Tree<String> implements Serializable {
    private static final long serialVersionUID = 5854124749688L;
    public static final int INDENT = 1;
    private static final int ONE = 1;

    Branch<String> root;

    public static class Branch<String> implements Serializable {
        private static final long serialVersionUID = 542412474573188L;

        String data;
        Branch<String> parent;
        List<Branch<String>> children = new ArrayList<>();

        public Branch(final String data, final Branch<String> parent) {
            this.data = data;
            this.parent = parent;
        }

        /**
         * Method print structure.
         *
         * @param indent - indent
         */
        public void print(int indent) {
            for (int i = ONE; i < indent; i++) {
                System.out.print("\t");
            }
            System.out.println(data);
            for (Branch child : children) {
                child.print(indent + ONE);
            }
        }
    }
}
