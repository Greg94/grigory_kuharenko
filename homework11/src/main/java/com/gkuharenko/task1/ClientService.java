package com.gkuharenko.task1;

import java.io.*;
import java.util.Scanner;

public class ClientService {

    private static final String EXIT = "0";
    private static final String SAVE = "1";
    private static final String LOAD = "2";
    private static final String DATA_FILE = "src/main/resources/data.txt";
    private static final String REGEX_PATH = "^(\\w.+)/*([^/]+)$";

    /**
     * Method launches the application.
     */
    public void runApp() {
        try (Scanner scanner = new Scanner(System.in)) {
            String path;
            while (true) {
                showWelcomeMSG();
                path = scanner.nextLine();
                if (path.equals(EXIT)) {
                    break;
                } else if (path.equals(SAVE)) {
                    savePath();
                } else if (path.equals(LOAD)) {
                    loadPath();
                } else {
                    if (checkPath(path)) {
                        FileSystemBuilder.buildFileSystem(path);
                    }
                }
                if (FileSystemBuilder.tree.root != null) {
                    FileSystemBuilder.tree.root.print(Tree.INDENT);
                }
            }
        }
    }

    /**
     * Method displays menu text.
     */
    private void showWelcomeMSG() {
        System.out.println("########MENU########");
        System.out.println("\t1 - Save");
        System.out.println("\t2 - Load");
        System.out.println("\t0 - Exit");
        System.out.println("########PATH########");
    }

    /**
     * Method serializes an object "tree"
     */
    private void savePath() {
        try (FileOutputStream stream = new FileOutputStream(DATA_FILE);
             ObjectOutputStream outputStream = new ObjectOutputStream(stream)) {
            outputStream.writeObject(FileSystemBuilder.tree);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method deserializes an object "tree"
     */
    private void loadPath() {
        try (FileInputStream stream = new FileInputStream(DATA_FILE);
             ObjectInputStream inputStream = new ObjectInputStream(stream)) {
            FileSystemBuilder.tree = (Tree<String>) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method checks if the string matches the regular expression of the path
     *
     * @param path - path
     * @return true if equal
     */
    private boolean checkPath(String path) {
        return path.matches(REGEX_PATH);
    }

}
