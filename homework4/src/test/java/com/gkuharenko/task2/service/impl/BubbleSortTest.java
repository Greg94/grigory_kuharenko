package com.gkuharenko.task2.service.impl;

import com.gkuharenko.task2.service.api.Sorter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class BubbleSortTest {

    private Sorter sorter;
    private int[] mas = {23, 14, 22, 21, 1, 2, 5, 7, 4, 9};

    @Before
    public void setUp() throws Exception {
        this.sorter = new BubbleSort();
    }

    @Test
    public void testSort() {
        int[] expected = mas.clone();
        Arrays.sort(expected);
        Assert.assertArrayEquals(expected, sorter.sort(mas));
    }
}