package com.gkuharenko.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DebitCardTest {

    private Card card;

    @Before
    public void setUp() throws Exception {
        card = new DebitCard("test", BigDecimal.valueOf(100));
    }

    @Test
    public void testWithdraw() {
        card.withdraw(BigDecimal.valueOf(45));
        Assert.assertEquals(BigDecimal.valueOf(55).setScale(2,BigDecimal.ROUND_UP),card.getAccountBalance());
    }

    @Test
    public void testWithdrawNegativeBalance() {
        card.withdraw(BigDecimal.valueOf(101));
        Assert.assertEquals(BigDecimal.valueOf(100).setScale(2,BigDecimal.ROUND_UP),card.getAccountBalance());
    }

    @Test
    public void testWithdrawBalanceZero() {
        card.withdraw(BigDecimal.valueOf(100));
        Assert.assertEquals(BigDecimal.valueOf(0).setScale(2,BigDecimal.ROUND_UP),card.getAccountBalance());
    }




}