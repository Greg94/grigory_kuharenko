package com.gkuharenko.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CardTest {

    private Card card;

    @Before
    public void setUp() throws Exception {
        this.card = new CreditCard("test", BigDecimal.valueOf(100));
    }

    @Test
    public void testAddToCardBalance() {
        card.addToCardBalance(BigDecimal.valueOf(33));
        Assert.assertEquals(BigDecimal.valueOf(133).setScale(2,BigDecimal.ROUND_UP), card.getAccountBalance());
    }

    @Test
    public void testAddToCardBalanceDouble() {
        card.addToCardBalance(BigDecimal.valueOf(12.24));
        Assert.assertEquals(BigDecimal.valueOf(112.24).setScale(2,BigDecimal.ROUND_UP), card.getAccountBalance());
    }

    @Test
    public void testWithdraw() {
        card.withdraw(BigDecimal.valueOf(25));
        Assert.assertEquals(BigDecimal.valueOf(75).setScale(2, BigDecimal.ROUND_UP), card.getAccountBalance());
    }

    @Test
    public void withdrawNegativeBalance() {
        card.withdraw(BigDecimal.valueOf(125));
        Assert.assertEquals(BigDecimal.valueOf(-25).setScale(2, BigDecimal.ROUND_UP), card.getAccountBalance());
    }


    @Test
    public void testConversionFromRublesToDollars() {
        BigDecimal expected = BigDecimal.valueOf(48.55);
        Assert.assertEquals(expected, card.conversionFromRublesToDollars());
    }
}