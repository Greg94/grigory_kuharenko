package com.gkuharenko.task2;

public class TaskPresentation2 {

    /**
     * Method launches the application.
     */
    public static void main(String[] args) {
        int[] array = {4, 42, 2, 21, 100, 21, 12, 1, 3, 81};
        new SortingContext().doChoiceSort(array);
    }

}
