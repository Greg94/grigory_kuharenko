package com.gkuharenko.task2;

import com.gkuharenko.task2.service.api.Sorter;
import com.gkuharenko.task2.service.impl.BubbleSort;
import com.gkuharenko.task2.service.impl.SelectionSort;

import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Supplier;

public class SortingContext {
    private static final String ERROR_CHOICE_SORT = "Please choice sort 1 or 2!";
    private static final String BUBBLE_SORT = "1";
    private static final String SELECTION_SORT = "2";

    private Sorter sorter;

    private HashMap<String, Supplier<Sorter>> sortMap = new HashMap<String, Supplier<Sorter>>() {
        {
            put("1", BubbleSort::new);
            put("2", SelectionSort::new);
        }
    };

    /**
     * Uses the selected view based on the sorter
     *
     * @param array - array to sort
     */
    private void executeSort(int[] array) {
        sorter.sort(array);
    }

    /**
     * Based on user input, select the sorting method
     *
     * @param array - array to sort
     */
    public void doChoiceSort(int[] array) {
        boolean isSelected = Boolean.TRUE;
        try (Scanner scanner = new Scanner(System.in)) {
            while (isSelected) {
                showSelectSort();
                String input = scanner.next();
                if (BUBBLE_SORT.equals(input) || SELECTION_SORT.equals(input)) {
                    sorter = sortMap.get(input).get();
                    executeSort(array);
                    isSelected = Boolean.FALSE;
                } else {
                    System.err.println(ERROR_CHOICE_SORT);
                }
            }
        }
    }

    /**
     * Method show select sort.
     */
    private void showSelectSort() {
        System.out.println("Please select sort");
        System.out.println("1 - Bubble sort");
        System.out.println("2 - Selection sort");
    }

}
