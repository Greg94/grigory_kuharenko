package com.gkuharenko.task2.service.api;

public interface Sorter {

    int FIRST_INDEX = 0;
    int SECOND_INDEX = 1;

    /**
     * method sorts an array
     *
     * @param array - array to sort
     * @return
     */
    int[] sort(int[] array);

}
