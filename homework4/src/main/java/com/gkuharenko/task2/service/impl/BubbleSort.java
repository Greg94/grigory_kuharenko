package com.gkuharenko.task2.service.impl;

import com.gkuharenko.task2.service.api.Sorter;

import java.util.Arrays;

public class BubbleSort implements Sorter {

    /**
     * Bubble sorts
     *
     * @param array - array to sort
     * @return - sort array
     */
    @Override
    public int[] sort(int[] array) {
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        System.out.println("Bubble sort: " + Arrays.toString(array));
        return array;
    }

}
