package com.gkuharenko.task2.service.impl;

import com.gkuharenko.task2.service.api.Sorter;

import java.util.Arrays;

public class SelectionSort implements Sorter {

    /**
     * Selection sorts
     *
     * @param array - array to sort
     * @return - sort array
     */
    @Override
    public int[] sort(int[] array) {
        for (int min = 0; min < array.length - 1; min++) {
            int least = min;
            for (int j = min + 1; j < array.length; j++) {
                if (array[j] < array[least]) {
                    least = j;
                }
            }
            int tmp = array[min];
            array[min] = array[least];
            array[least] = tmp;
        }
        System.out.println("Selection sort: " + Arrays.toString(array));
        return array;
    }

}
