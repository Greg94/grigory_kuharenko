package com.gkuharenko.task1;

import java.math.BigDecimal;

public class DebitCard extends Card {

    private static final String ERROR_NEGATIVE_BALANCE = "This is debit card. Negative balance is not allowed!";

    public DebitCard(String cardOwner, BigDecimal accountBalance) {
        super(cardOwner, accountBalance);
    }

    public DebitCard(String cardOwner) {
        super(cardOwner);
    }

    /**
     * Shows debit card status
     */
    @Override
    public void getFromCardBalance() {
        System.out.printf("This is debit card. Card owner: %s, current balance: %s.\n", getCardOwner(), getAccountBalance());
    }

    /**
     * Withdraw money with card. Does not allow withdrawals if this leads to a negative balance on the card.
     *
     * @param money - transfer the amount of money
     */
    @Override
    public void withdraw(BigDecimal money) {
        if ((getAccountBalance().subtract(money).doubleValue()) >= 0) {
            super.withdraw(money);
        } else {
            System.err.println(ERROR_NEGATIVE_BALANCE);
        }
    }

}
