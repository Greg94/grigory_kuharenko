package com.gkuharenko.task1;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.function.Supplier;


public class Atm {

    private static final String DEBIT_CARD_VASYA = "1";
    private static final String CREDIT_CARD_PETYA = "2";
    private static final String SHOW_BALANCE = "1";
    private static final String ADD_MONEY = "2";
    private static final String WITHDRAW = "3";
    private static final String CONVERSION = "4";
    private static final String EXIT = "0";
    private static final String ERROR_INPUT = "Incorrect input!";
    private static final String ERROR_CHOICE_CARD = "Please choice card 1 or 2";

    private Card card;

    private HashMap<String, Supplier<Card>> cardMap = new HashMap<String, Supplier<Card>>() {
        {
            put(DEBIT_CARD_VASYA, () -> new DebitCard("Vasya", BigDecimal.valueOf(1000)));
            put(CREDIT_CARD_PETYA, () -> new CreditCard("Petya", BigDecimal.valueOf(300)));
        }
    };

    /**
     * The method simulates the operation of an ATM. Scanner is used to process input.
     */
    public void startPresentation() {
        try (Scanner scanner = new Scanner(System.in)) {
            doCardChoice(scanner);
            showMenu(scanner);
        }
    }

    /**
     * The method provides a choice of cards
     *
     * @param scanner - choice cards Credit or Debit
     */
    private void doCardChoice(Scanner scanner) {
        boolean isCardNotSelected = Boolean.TRUE;
        while (isCardNotSelected) {
            showSelectCardMsg();
            String input = scanner.next();

            if (DEBIT_CARD_VASYA.equals(input) || CREDIT_CARD_PETYA.equals(input)) {
                this.card = cardMap.get(input).get();
                isCardNotSelected = Boolean.FALSE;
            } else {
                System.err.println(ERROR_CHOICE_CARD);
            }
        }
    }

    /**
     * Method show msg choice cards.
     */
    private void showSelectCardMsg() {
        System.out.println("Please select card");
        System.out.println("1 - Debit card Vasya");
        System.out.println("2 - Credit card Petya");
    }

    /**
     * Shows a menu for selecting operation.
     *
     * @param scanner - choice operation
     */
    private void showMenu(Scanner scanner) {
        boolean isExit = false;
        while (!isExit) {
            showWelcomeMsg();
            String input = scanner.next();
            isExit = doOperationWithCard(scanner, isExit, input);
        }
    }

    /**
     * Method show menu.
     */
    private void showWelcomeMsg() {
        System.out.println("You choose " + card);
        System.out.println("########### MENU ##############");
        System.out.println("1 - show balance");
        System.out.println("2 - add money");
        System.out.println("3 - withdraw money");
        System.out.println("4 - conversion money BYN to USA");
        System.out.println("0 - exit");
    }

    /**
     * Method choice operations with card.
     *
     * @param scanner - amount of money
     * @param isExit  - loop exit flag
     * @param input   - choice operation
     * @return true or false
     */
    private boolean doOperationWithCard(Scanner scanner, Boolean isExit, String input) {
        if (SHOW_BALANCE.equals(input)) {
            this.card.getFromCardBalance();
        } else if (ADD_MONEY.equals(input)) {
            addMoney(scanner);
        } else if (WITHDRAW.equals(input)) {
            withdrawMoney(scanner);
        } else if (CONVERSION.equals(input)) {
            this.card.conversionFromRublesToDollars();
        } else if (EXIT.equals(input)) {
            isExit = Boolean.TRUE;
        } else {
            System.out.println("Please make correct choice!");
        }
        return isExit;
    }

    /**
     * Method add money using scanner
     * Scanner does not allow to enter a negative number.
     *
     * @param scanner - transfer the amount of money
     */
    private void addMoney(Scanner scanner) {
        boolean isInputCorrect = Boolean.FALSE;
        while (!isInputCorrect) {
            System.out.println("How much money do you want to add?");
            try {
                this.card.addToCardBalance(scanner.nextBigDecimal().abs());
                isInputCorrect = Boolean.TRUE;
            } catch (InputMismatchException e) {
                System.err.println(ERROR_INPUT);
                return;
            }
        }
    }

    /**
     * Method withdraw money.
     * Scanner does not allow to enter a negative number.
     *
     * @param scanner - transfer the amount of money
     */
    private void withdrawMoney(Scanner scanner) {
        boolean isInputCorrect = Boolean.FALSE;
        while (!isInputCorrect) {
            System.out.println("How much money do you want to withdraw?");
            try {
                this.card.withdraw(scanner.nextBigDecimal().abs());
                isInputCorrect = Boolean.TRUE;
            } catch (InputMismatchException e) {
                System.err.println(ERROR_INPUT);
                return;
            }
        }
    }

}
