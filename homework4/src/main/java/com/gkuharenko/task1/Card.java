package com.gkuharenko.task1;

import java.math.BigDecimal;

public abstract class Card {

    private static final int SCALE_ROUND_UP = 2;
    private static final int EMPTY_BALANCE = 0;
    private static final double EXCHANGE_RATE = 2.06;

    private String cardOwner;
    private BigDecimal accountBalance;

    public Card(String cardOwner, BigDecimal accountBalance) {
        this.cardOwner = cardOwner;
        this.accountBalance = accountBalance;
    }

    public Card(String cardOwner) {
        this.cardOwner = cardOwner;
        this.accountBalance = new BigDecimal(EMPTY_BALANCE);
    }

    /**
     * Gets the field value {@link Card#cardOwner}.
     *
     * @return cardOwner.
     */
    public String getCardOwner() {
        return cardOwner;
    }

    /**
     * Gets the field value {@link Card#accountBalance}.
     *
     * @return accountBalance.
     */
    public BigDecimal getAccountBalance() {
        return accountBalance.setScale(SCALE_ROUND_UP, BigDecimal.ROUND_UP);
    }

    /**
     * Shows the status of the card
     */
    public void getFromCardBalance() {
        System.out.printf("Card owner: %s, current balance: %s.\n", cardOwner, accountBalance.setScale(SCALE_ROUND_UP, BigDecimal.ROUND_UP));
    }

    /**
     * Add money to card
     *
     * @param money - transfer the amount of money
     */
    public void addToCardBalance(BigDecimal money) {
        accountBalance = accountBalance.add(money);
        System.out.printf("%s rubles add to balance of %s's account.\n", money, cardOwner);
    }

    /**
     * Withdraw money with card.
     *
     * @param money - transfer the amount of money
     */
    public void withdraw(BigDecimal money) {
        accountBalance = accountBalance.subtract(money);
        System.out.printf("%s rubles removed from the balance of %s's account.\n", money, cardOwner);
    }

    /**
     * Currency conversion from rubles to dollars.
     *
     * @return balance after conversion.
     */
    public BigDecimal conversionFromRublesToDollars() {
        BigDecimal balanceInRubles = accountBalance;
        BigDecimal conversionToDollars = accountBalance.divide(BigDecimal.valueOf(EXCHANGE_RATE), SCALE_ROUND_UP, BigDecimal.ROUND_UP);
        System.out.printf("The balance in rubles is %s, in dollars is %s.\n", balanceInRubles, conversionToDollars);
        return conversionToDollars;
    }

    /**
     * Shows the state of an object
     *
     * @return status bar
     */
    @Override
    public String toString() {
        return "Card{" +
                "cardOwner='" + cardOwner + '\'' +
                ", accountBalance=" + accountBalance +
                '}';
    }
}
