package com.gkuharenko.task1;

import java.math.BigDecimal;

public class CreditCard extends Card {

    public CreditCard(String cardOwner, BigDecimal accountBalance) {
        super(cardOwner, accountBalance);
    }

    public CreditCard(String cardOwner) {
        super(cardOwner);
    }

    /**
     * shows credit card status
     */
    @Override
    public void getFromCardBalance() {
        System.out.printf("This is credit card. Card owner: %s, current balance: %s.\n", getCardOwner(), getAccountBalance());
    }
}
