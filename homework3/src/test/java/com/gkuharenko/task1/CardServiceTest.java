package com.gkuharenko.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CardServiceTest {
    private Card card1;
    private CardService cardService;

    @Before
    public void setUp() throws Exception {
        card1 = new Card("test1", BigDecimal.valueOf(1000));
        cardService = new CardService(card1);
    }

    @Test
    public void testGetFromCardBalance() {
        cardService.getFromCardBalance();
        BigDecimal expected = BigDecimal.valueOf(1000);
        Assert.assertEquals(expected.doubleValue(), card1.getAccountBalance().doubleValue(), 0);
    }

    @Test
    public void testAddToCardBalance() {
        cardService.addToCardBalance(BigDecimal.valueOf(123));
        BigDecimal expected = BigDecimal.valueOf(1123);
        Assert.assertEquals(expected.doubleValue(), card1.getAccountBalance().doubleValue(), 0);
    }

    @Test
    public void testAddToCardBalanceNegativeNumber() {
        cardService.addToCardBalance(BigDecimal.valueOf(-123));
        BigDecimal expected = BigDecimal.valueOf(1000);
        Assert.assertEquals(expected.doubleValue(), card1.getAccountBalance().doubleValue(), 0);
    }


    @Test
    public void testWithdraw() {
        cardService.withdraw(BigDecimal.valueOf(123));
        BigDecimal expected = BigDecimal.valueOf(877);
        Assert.assertEquals(expected.doubleValue(), card1.getAccountBalance().doubleValue(), 0);
    }

    @Test
    public void testWithdrawNegativeNumber() {
        cardService.withdraw(BigDecimal.valueOf(-324));
        BigDecimal expected = BigDecimal.valueOf(1000);
        Assert.assertEquals(expected.doubleValue(), card1.getAccountBalance().doubleValue(), 0);
    }

    @Test
    public void testConversion() {
        BigDecimal expected = BigDecimal.valueOf(485.44);
        Assert.assertEquals(expected.doubleValue(), cardService.conversionFromRublesToDollars(card1).doubleValue(),0);
    }
}