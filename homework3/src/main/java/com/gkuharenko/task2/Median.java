package com.gkuharenko.task2;

import java.util.Arrays;

public class Median {

    private Median() {

    }

    /**
     * Finds the median of an array of integers.
     *
     * @param mas - array of integers.
     * @return median.
     */
    public static float median(int[] mas) {
        Arrays.sort(mas);
        float median;
        if (mas.length % 2 == 0) {
            median = ((float) mas[mas.length / 2] + (float) mas[mas.length / 2 - 1]) / 2;
        } else {
            median = mas[mas.length / 2];
        }
        return median;
    }

    /**
     * Finds the median of an array of doubles
     *
     * @param mas - array of doubles.
     * @return median.
     */
    public static double median(double[] mas) {
        Arrays.sort(mas);
        double median;
        if (mas.length % 2 == 0) {
            median = (mas[mas.length / 2] + mas[mas.length / 2 - 1]) / 2;
        } else {
            median = mas[mas.length / 2];
        }
        return median;
    }

}
