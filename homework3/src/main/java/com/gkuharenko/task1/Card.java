package com.gkuharenko.task1;

import java.math.BigDecimal;

public class Card {

    private static final int SCALE_ROUND_UP = 2;
    private static final int EMPTY_BALANCE = 0;

    private String cardOwner;
    private BigDecimal accountBalance;

    public Card(String cardOwner, BigDecimal accountBalance) {
        this.cardOwner = cardOwner;
        this.accountBalance = accountBalance;
    }

    public Card(String cardOwner) {
        this.cardOwner = cardOwner;
        this.accountBalance = new BigDecimal(EMPTY_BALANCE);
    }

    /**
     * Gets the field value {@link Card#cardOwner}.
     *
     * @return cardOwner.
     */
    public String getCardOwner() {
        return cardOwner;
    }


    /**
     * Gets the field value {@link Card#accountBalance}.
     *
     * @return accountBalance.
     */
    public BigDecimal getAccountBalance() {
        return accountBalance.setScale(2, BigDecimal.ROUND_UP);
    }

    /**
     * Determines the balance
     *
     * @param accountBalance - set balance
     */
    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance.setScale(SCALE_ROUND_UP, BigDecimal.ROUND_UP);
    }

}
