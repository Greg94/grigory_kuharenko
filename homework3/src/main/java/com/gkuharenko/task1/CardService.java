package com.gkuharenko.task1;

import java.math.BigDecimal;

public class CardService {

    private Card card;

    private static final String VALUES_ERROR = "Money cannot have a negative value!";
    private static final double EXCHANGE_RATE = 2.06;

    public CardService(Card card) {
        this.card = card;
    }

    /**
     * Gets the current card balance.
     */
    public void getFromCardBalance() {
        System.out.printf("Card owner: %s, current balance: %s.\n", card.getCardOwner(), card.getAccountBalance());
    }

    /**
     * Adds money to the card
     *
     * @param money - money
     */
    public void addToCardBalance(BigDecimal money) {
        if (isValidate(money)) {
            BigDecimal newBalance = card.getAccountBalance().add(money);
            card.setAccountBalance(newBalance);
            System.out.printf("%s rubles were credited to %s's account balance.\n", money, card.getCardOwner());
        }
    }

    /**
     * Withdraws money from a card.
     *
     * @param money - money
     */
    public void withdraw(BigDecimal money) {
        if (isValidate(money)) {
            BigDecimal newBalance = card.getAccountBalance().subtract(money);
            card.setAccountBalance(newBalance);
            System.out.printf("%s rubles removed from the balance of %s's account.\n", money, card.getCardOwner());
        }
    }

    /**
     * Currency conversion from rubles to dollars.
     *
     * @param card - card
     * @return balance after conversion.
     */
    public BigDecimal conversionFromRublesToDollars(Card card) {
        BigDecimal balanceInRubles = card.getAccountBalance();
        BigDecimal conversionToDollars = card.getAccountBalance().divide(BigDecimal.valueOf(EXCHANGE_RATE), BigDecimal.ROUND_UP);
        System.out.printf("The balance in rubles is %s, in dollars is %s", balanceInRubles, conversionToDollars);
        return conversionToDollars;
    }

    /**
     * Checks that the specified money is greater than zero.
     *
     * @param money - money
     * @return true if value is greater than zero, else false.
     */
    private boolean isValidate(BigDecimal money) {
        if (money.doubleValue() > 0) {
            return true;
        } else {
            System.err.println(VALUES_ERROR);
            return false;
        }
    }

}