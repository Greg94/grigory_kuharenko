package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.domain.Role;
import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.exception.UserNotFoundException;
import com.gkuharenko.task1.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void testGetUserByLogin() {
        //given
        String login = "test";
        User user = new User(1L, "test", "test", Role.ADMIN);

        when(userRepository.findUserByLogin(login)).thenReturn(Optional.of(user));

        //when
        Optional<User> userResult = Optional.ofNullable(userService.getUserByLogin(login));

        //then
        Assert.assertTrue(userResult.isPresent());
        Assert.assertEquals(user, userResult.get());

        verify(userRepository).findUserByLogin(login);
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetUserByLoginFails() {
        //given
        String login = "test";
        User user = new User(99L, "test", "test", Role.ADMIN);

        when(userRepository.findUserByLogin(login)).thenThrow(UserNotFoundException.class);

        //when
        userService.getUserByLogin(login);

        //then
        verify(userRepository).findUserByLogin(login);
    }

}