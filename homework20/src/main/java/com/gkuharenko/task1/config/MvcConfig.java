package com.gkuharenko.task1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {"com.gkuharenko.task1.controller",
        "com.gkuharenko.task1.repository",
        "com.gkuharenko.task1.service",
        "com.gkuharenko.task1.exception.handler"})
public class MvcConfig implements WebMvcConfigurer {

    private static final String VIEW_RESOLVER_PREFIX = "/WEB-INF/views/";
    private static final String VIEW_RESOLVER_SUFFIX = ".jsp";
    private static final String RESOURCE_LOCATION = "/views/";
    private static final String PATH_PATTERN = "/WEB-INF/views/**";

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(PATH_PATTERN).addResourceLocations(RESOURCE_LOCATION);
    }


    @Bean
    public InternalResourceViewResolver setupViewResolver() {
        final InternalResourceViewResolver resolver =
                new InternalResourceViewResolver(VIEW_RESOLVER_PREFIX, VIEW_RESOLVER_SUFFIX);
        resolver.setViewClass(JstlView.class);
        return resolver;
    }

}
