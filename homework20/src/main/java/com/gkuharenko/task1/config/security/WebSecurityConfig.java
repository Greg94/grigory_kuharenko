package com.gkuharenko.task1.config.security;

import com.gkuharenko.task1.service.impl.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@ComponentScan(basePackages = "config.security")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService detailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(detailsService).passwordEncoder(encoder());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.cors()
                .disable()
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/online-shop")
                .permitAll()
                .antMatchers("/error")
                .permitAll()
                .antMatchers("/online-shop/**")
                .hasAnyRole("ADMIN", "USER")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/online-shop")
                .failureUrl("/error")
                .loginProcessingUrl("/sign-in")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/online-shop/items", true)
                .and()
                .logout()
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/online-shop")
                .permitAll()
                .and()
                .sessionManagement()
                .invalidSessionUrl("/online-shop")
                .maximumSessions(1)
                .expiredUrl("/online-shop");
    }

    private PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}
