package com.gkuharenko.task1.repository;


import com.gkuharenko.task1.domain.Item;

import java.util.List;
import java.util.Optional;

public interface ItemRepository {

    /**
     * Receives a list of items from the database
     *
     * @return {@link List<Item>}
     */
    List<Item> listItems();

    /**
     * Search items in the database by id
     *
     * @param id item id
     * @return {@link Optional<Item>}
     */
    Optional<Item> findItemById(String id);

}
