package com.gkuharenko.task1.repository;


import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.domain.Order;
import com.gkuharenko.task1.domain.User;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface OrderRepository {

    /**
     * Adds item to order
     *
     * @param order order
     * @param item  item
     */
    void addItemToOrder(Order order, Item item);

    /**
     * Search products in the order database
     *
     * @param order order
     * @return {@link Optional<List>}
     */
    Optional<List<Item>> findItemsByOrder(Order order);

    /**
     * Saves the order in the database
     *
     * @param user       user
     * @param totalPrice total price
     */
    void createOrder(User user, BigDecimal totalPrice);

    /**
     * Search a user order in the database
     *
     * @param user user
     * @return {@link Optional<Order>}
     */
    Optional<Order> findOrderByUser(User user);

    /**
     * Updates the total price of the order
     *
     * @param order      order
     * @param totalPrice total price
     */
    void updateTotalPriceOrder(Order order, BigDecimal totalPrice);

}
