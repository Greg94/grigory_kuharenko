package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.exception.UserNotFoundException;
import com.gkuharenko.task1.repository.UserRepository;
import com.gkuharenko.task1.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUserByLogin(String login) {
        return userRepository.findUserByLogin(login).orElseThrow(UserNotFoundException::new);
    }
}
