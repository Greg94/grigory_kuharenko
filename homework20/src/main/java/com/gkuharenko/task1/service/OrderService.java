package com.gkuharenko.task1.service;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.domain.Order;
import com.gkuharenko.task1.domain.User;

import java.util.List;


public interface OrderService {

    /**
     * save items in order
     *
     * @param user  user
     * @param items items
     */
    void saveResultOrder(User user, List<Item> items);

    /**
     * Get items by order
     *
     * @param order order
     * @return {@link List<Item>}
     */
    List<Item> getItemsByOrder(Order order);

    /**
     * Get order by user
     *
     * @param user user
     * @return {@link Order}
     */
    Order getOrderByUser(User user);
}
