package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.domain.Order;
import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.repository.OrderRepository;
import com.gkuharenko.task1.service.OrderService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private static final BigDecimal START_TOTAL_PRICE = BigDecimal.valueOf(0.0);

    private OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public void saveResultOrder(User user, List<Item> items) {
        Order order = getOrderByUser(user);
        if (order == null) {
            orderRepository.createOrder(user, START_TOTAL_PRICE);
        }
        order = getOrderByUser(user);
        for (Item item : items) {
            orderRepository.addItemToOrder(order, item);
            order.setTotalPrice(order.getTotalPrice().add(item.getPrice()));
        }
        orderRepository.updateTotalPriceOrder(order, order.getTotalPrice());
    }

    @Override
    public List<Item> getItemsByOrder(Order order) {
        return orderRepository.findItemsByOrder(order).orElseThrow(RuntimeException::new);
    }

    @Override
    public Order getOrderByUser(User user) {
        return orderRepository.findOrderByUser(user).orElse(null);
    }
}
