package com.gkuharenko.task1.exception.handler;

import com.gkuharenko.task1.exception.UserNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class UserExceptionHandler {

    private static final String ERROR_VIEW_NAME = "index";
    private static final String ERROR_ATTRIBUTE_NAME = "error";

    @ExceptionHandler(UserNotFoundException.class)
    public ModelAndView handleUserNotFoundException(final UserNotFoundException exception) {
        return new ModelAndView(ERROR_VIEW_NAME)
                .addObject(ERROR_ATTRIBUTE_NAME, exception.getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ModelAndView handleUsernameNotFoundException(final UsernameNotFoundException exception) {
        return new ModelAndView(ERROR_VIEW_NAME)
                .addObject(ERROR_ATTRIBUTE_NAME, exception.getMessage());
    }

}
