package com.gkuharenko.task1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorController {

    private static final String ERROR_PATH = "/errors";
    private static final String ERROR_VIEW = "errorPage";
    private static final String ERROR_400_MSG = "Http Error Code: 400. Bad Request";
    private static final String ERROR_401_MSG = "Http Error Code: 401. Unauthorized";
    private static final String ERROR_404_MSG = "Http Error Code: 404. Resource not found";
    private static final String ERROR_500_MSG = "Http Error Code: 500. Internal Server Error";
    private static final String ERROR_MSG = "errorMsg";
    private static final String STATUS_CODE_PATH = "javax.servlet.error.status_code";

    @GetMapping(ERROR_PATH)
    public ModelAndView renderErrorPage(HttpServletRequest httpRequest) {
        ModelAndView errorPage = new ModelAndView(ERROR_VIEW);
        String errorMsg = "";
        int httpErrorCode = getErrorCode(httpRequest);
        switch (httpErrorCode) {
            case 400: {
                errorMsg = ERROR_400_MSG;
                break;
            }
            case 401: {
                errorMsg = ERROR_401_MSG;
                break;
            }
            case 404: {
                errorMsg = ERROR_404_MSG;
                break;
            }
            case 500: {
                errorMsg = ERROR_500_MSG;
                break;
            }
        }
        errorPage.addObject(ERROR_MSG, errorMsg);
        return errorPage;
    }

    private int getErrorCode(HttpServletRequest httpRequest) {
        return (Integer) httpRequest
                .getAttribute(STATUS_CODE_PATH);
    }

}