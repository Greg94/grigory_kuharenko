package com.gkuharenko.task1.controller;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.domain.Order;
import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.service.OrderService;
import com.gkuharenko.task1.service.UserService;
import com.gkuharenko.task1.service.impl.CustomUserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

import static com.gkuharenko.task1.controller.ItemController.ONLINE_SHOP_PATH;
import static com.gkuharenko.task1.controller.ItemController.USER_ATTRIBUTE;
import static com.gkuharenko.task1.service.impl.ItemServiceImpl.RESULT;

@Controller
@RequestMapping(ONLINE_SHOP_PATH)
public class OrderController {

    private static final String TOTAL_PRICE = "totalPrice";
    private static final String ORDER_ITEMS = "orderItems";
    private static final String ORDER_PATH = "/order";
    private static final String ORDER_VIEW = "order";

    private OrderService orderService;
    private UserService userService;
    private CustomUserDetailsService userDetailsService;

    public OrderController(OrderService orderService, UserService userService, CustomUserDetailsService userDetailsService) {
        this.orderService = orderService;
        this.userService = userService;
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, path = ORDER_PATH)
    public String showOrder(HttpServletRequest req, Model model) {
        HttpSession httpSession = req.getSession();
        model.addAttribute(USER_ATTRIBUTE, userDetailsService.getPrincipal());
        List<Item> resultList = (List<Item>) httpSession.getAttribute(RESULT);
        User user = userService.getUserByLogin(userDetailsService.getPrincipal());
        if (resultList != null) {
            orderService.saveResultOrder(user, resultList);
        }
        Order order = orderService.getOrderByUser(user);
        if (order != null) {
            List<Item> orderItems = orderService.getItemsByOrder(order);
            model.addAttribute(ORDER_ITEMS, orderItems);
            BigDecimal totalPrice = order.getTotalPrice();
            model.addAttribute(TOTAL_PRICE, totalPrice);
        }
        return ORDER_VIEW;
    }
}
