package com.gkuharenko.task1.controller;

import com.gkuharenko.task1.exception.UserNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    private static final String LOGIN_PATH = "/online-shop";
    private static final String LOGIN_VIEW = "index";
    private static final String ERROR_PATH = "/error";

    @GetMapping(LOGIN_PATH)
    public String loginPage() {
        return LOGIN_VIEW;
    }

    @GetMapping(ERROR_PATH)
    public void loginError() {
        throw new UserNotFoundException();
    }

}
