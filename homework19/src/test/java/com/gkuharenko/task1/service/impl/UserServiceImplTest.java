package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void getUserByLogin() {
        //given
        String login = "test";
        User user = new User(1L, "test", "test");

        when(userRepository.findUserByLogin(login)).thenReturn(java.util.Optional.of(user));

        //when
        Optional<User> userResult = Optional.ofNullable(userService.getUserByLogin(login));

        //then
        Assert.assertTrue(userResult.isPresent());
        Assert.assertEquals(user, userResult.get());

        verify(userRepository).findUserByLogin(login);
    }

    @Test(expected = NullPointerException.class)
    public void getUserByLoginFails() {
        //given
        String login = "test";
        User user = new User(99L, "test", "test");

        when(userRepository.findUserByLogin(login)).thenReturn(null);

        //when
        userService.getUserByLogin(login);

        //then
        verify(userRepository).findUserByLogin(login);
    }

}