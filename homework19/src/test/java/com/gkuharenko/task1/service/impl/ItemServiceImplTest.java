package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.repository.ItemRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ItemServiceImplTest {

    @Mock
    private ItemRepository itemRepository;
    @InjectMocks
    private ItemServiceImpl itemService;

    @Test
    public void testDoListResult() {
        //given
        String id = "3";
        HttpSession httpSession = mock(HttpSession.class);
        List<Item> itemListTest = new ArrayList<>();
        itemListTest.add(new Item(1L, "Book", 5.5));
        itemListTest.add(new Item(4L, "Laptop", 120.45));

        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item(1L, "Book", 5.5));
        itemList.add(new Item(4L, "Laptop", 120.45));
        itemList.add(new Item(3L, "Coffee Machine", 100));

        when(httpSession.getAttribute("result")).thenReturn(itemListTest);
        when(itemRepository.findItemById(id)).thenReturn(java.util.Optional.of(new Item(3L, "Coffee Machine", 100)));

        //when
        Optional<List<Item>> result = Optional.ofNullable(itemService.doTemporaryListItems(httpSession, id));

        //then
        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(itemList, result.get());

        verify(itemRepository).findItemById(id);
    }

    @Test
    public void testGetItemById() {
        //given
        String id = "1";
        Item testItem = new Item(1L, "Book", 5.5);
        when(itemRepository.findItemById("1")).thenReturn(Optional.of(testItem));

        //when
        Optional<Item> itemResult = Optional.ofNullable(itemService.getItemById(id));

        //then
        Assert.assertTrue(itemResult.isPresent());
        Assert.assertEquals(testItem, itemResult.get());

        verify(itemRepository).findItemById(id);
    }

    @Test(expected = RuntimeException.class)
    public void testGetItemByIdFails() {
        //given
        String id = "99";
        when(itemRepository.findItemById("99")).thenThrow(RuntimeException.class);

        //when
        itemService.getItemById(id);

        //then
        verify(itemRepository).findItemById(id);
    }


}