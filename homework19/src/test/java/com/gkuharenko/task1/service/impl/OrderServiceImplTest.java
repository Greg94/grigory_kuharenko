package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.domain.Order;
import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.repository.OrderRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;
    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void testGetItemsByOrder() {
        //given
        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item(1L, "Book", 5.5));
        itemList.add(new Item(4L, "Laptop", 120.45));
        itemList.add(new Item(3L, "Coffee Machine", 100));

        Order testOrder = new Order(1L, 1L, 100);
        when(orderRepository.findItemsByOrder(testOrder)).thenReturn(java.util.Optional.of(itemList));

        //when
        Optional<List<Item>> itemsResult = Optional.ofNullable(orderService.getItemsByOrder(testOrder));

        //then
        Assert.assertTrue(itemsResult.isPresent());
        Assert.assertEquals(itemList, itemsResult.get());
    }

    @Test(expected = RuntimeException.class)
    public void testGetItemsByOrderFails() {
        //given
        Order testOrder = new Order(99L, 1L, 100);
        when(orderRepository.findItemsByOrder(testOrder)).thenThrow(RuntimeException.class);

        //when
        orderService.getItemsByOrder(testOrder);

        //then
        verify(orderRepository).findItemsByOrder(testOrder);
    }

    @Test
    public void testGetOrderByUser() {
        //given
        User user = new User(1L, "test", "test");
        Order testOrder = new Order(1L, 1L, 100);
        when(orderRepository.findOrderByUser(user)).thenReturn(Optional.of(testOrder));

        //when
        Optional<Order> orderResult = Optional.ofNullable(orderService.getOrderByUser(user));

        //then
        Assert.assertTrue(orderResult.isPresent());
        Assert.assertEquals(testOrder, orderResult.get());
    }

    @Test(expected = NullPointerException.class)
    public void testGetOrderByUserFails() {
        //given
        User user = new User(99L, "test", "test");
        when(orderRepository.findOrderByUser(user)).thenReturn(null);

        //when
        orderService.getOrderByUser(user);

        //then
        verify(orderRepository).findOrderByUser(user);
    }


}