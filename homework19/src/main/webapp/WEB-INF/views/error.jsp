<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>

<head>
    <title>Oops!</title>
</head>

<body align="center">

    <h1>Oops!</h1>
    <p>You should`t be here</p>
    <p>Please, agree with the terms of service first</p>
    <p>or check the login.</p>
    <p><a href="/online-shop">Start page</a></p>

</body>

</html>