<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>

<head>
    <title>Order</title>
</head>

<body align="center">

    <h1>Dear ${authUser.login}, your order: </h1>

    <c:if test="${not empty orderItems}">
        <c:set var="count" value="0" scope="page" />
        <c:forEach items="${orderItems}" var="item">
            <c:set var="count" value="${count+1}" scope="page" />
            <p>
                <tr>
                    <td>${count})</td>
                    <td>${item.name} </td>
                    <td>${item.price}$</td>
                </tr>
            </p>
        </c:forEach>
        Total: ${totalPrice}$.
    </c:if>
    <c:if test="${empty orderItems}">
        <p>No product selected</p>
    </c:if>

    <p><a href="/logout">Logout</a></p>

</body>

</html>