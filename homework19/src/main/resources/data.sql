CREATE TABLE IF NOT EXISTS Users (id int NOT NULL AUTO_INCREMENT, login VARCHAR(255), password VARCHAR(255), PRIMARY KEY(ID));
CREATE TABLE IF NOT EXISTS Items (id int NOT NULL AUTO_INCREMENT, name VARCHAR(255), price double, PRIMARY KEY(ID));
CREATE TABLE IF NOT EXISTS Orders (id int NOT NULL AUTO_INCREMENT, user_id int, FOREIGN KEY(user_id) REFERENCES Users(id), total_price double);
CREATE TABLE IF NOT EXISTS Order_Items (order_id int, item_id int, FOREIGN KEY (order_id) REFERENCES Orders(id), FOREIGN KEY (item_id) REFERENCES Items(id));

INSERT INTO Users(login, password) SELECT 'admin', 'admin' FROM DUAL WHERE NOT EXISTS(SELECT * FROM Users WHERE Users.login='admin');
INSERT INTO Users(login, password) SELECT 'user', 'user' FROM DUAL WHERE NOT EXISTS(SELECT * FROM Users WHERE Users.login='user');
INSERT INTO Items(name, price) SELECT 'Book', '5.5' FROM DUAL WHERE NOT EXISTS(SELECT * FROM Items WHERE Items.name='Book');
INSERT INTO Items(name, price) SELECT 'Mobile Phone', '60.8' FROM DUAL WHERE NOT EXISTS(SELECT * FROM Items WHERE Items.name='Mobile Phone');
INSERT INTO Items(name, price) SELECT 'Coffee Machine', '100' FROM DUAL WHERE NOT EXISTS(SELECT * FROM Items WHERE Items.name='Coffee Machine');
INSERT INTO Items(name, price) SELECT 'Laptop', '120.45' FROM DUAL WHERE NOT EXISTS(SELECT * FROM Items WHERE Items.name='Laptop');
INSERT INTO Items(name, price) SELECT 'Cup', '3.2' FROM DUAL WHERE NOT EXISTS(SELECT * FROM Items WHERE Items.name='Cup');

