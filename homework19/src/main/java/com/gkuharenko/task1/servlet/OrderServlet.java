package com.gkuharenko.task1.servlet;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.domain.Order;
import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.service.OrderService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.gkuharenko.task1.filter.LoginFilter.USER;
import static com.gkuharenko.task1.service.impl.ItemServiceImpl.RESULT;

@WebServlet(urlPatterns = "/order")
public class OrderServlet extends HttpServlet {


    private static final String TOTAL_PRICE = "totalPrice";
    private static final String ORDER_PATH = "WEB-INF/views/order.jsp";
    private static final String ORDER_ITEMS = "orderItems";

    private OrderService orderService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        this.orderService = (OrderService) context.getBean("orderService");
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession httpSession = req.getSession();
        List<Item> resultList = (List<Item>) httpSession.getAttribute(RESULT);
        User user = (User) httpSession.getAttribute(USER);
        if (resultList != null) {
            orderService.saveResultOrder(user, resultList);
        }
        Order order = orderService.getOrderByUser(user);
        if (order != null) {
            List<Item> orderItems = orderService.getItemsByOrder(order);
            req.setAttribute(ORDER_ITEMS, orderItems);
            double totalPrice = order.getTotalPrice();
            req.setAttribute(TOTAL_PRICE, totalPrice);
        }
        req.getRequestDispatcher(ORDER_PATH).forward(req, resp);
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
