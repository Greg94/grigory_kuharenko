package com.gkuharenko.task1.servlet;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.service.ItemService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet(urlPatterns = "/items")
public class ItemServlet extends HttpServlet {

    private static final String ADD_PARAMETER = "addItem";
    private static final String ITEMS_PATH = "WEB-INF/views/items.jsp";
    private static final String LIST_ITEMS = "listItems";

    private ItemService itemService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        itemService = (ItemService) context.getBean("itemService");
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession httpSession = req.getSession();
        String itemId = req.getParameter(ADD_PARAMETER);
        List<Item> allItems = itemService.getAllItems();
        itemService.doTemporaryListItems(httpSession, itemId);
        req.setAttribute(LIST_ITEMS, allItems);
        req.getRequestDispatcher(ITEMS_PATH).forward(req, resp);
    }
}
