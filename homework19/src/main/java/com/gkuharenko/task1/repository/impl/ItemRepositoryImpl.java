package com.gkuharenko.task1.repository.impl;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.mapper.ItemMapper;
import com.gkuharenko.task1.repository.ItemRepository;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Optional;

public class ItemRepositoryImpl implements ItemRepository {

    private static final String SQL_SELECT_ALL_ITEMS = "SELECT * FROM Items";
    private static final String SQL_FIND_ITEM_BY_ID = "SELECT * FROM Items AS i WHERE i.id= ?";

    private JdbcTemplate jdbcTemplate;

    public ItemRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Item> listItems() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ITEMS, new ItemMapper());
    }

    @Override
    public Optional<Item> findItemById(String id) {
        return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_FIND_ITEM_BY_ID, new Object[]{Long.parseLong(id)}, new ItemMapper()));
    }

}
