package com.gkuharenko.task1.repository.impl;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.domain.Order;
import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.mapper.ItemMapper;
import com.gkuharenko.task1.mapper.OrderMapper;
import com.gkuharenko.task1.repository.OrderRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Optional;

public class OrderRepositoryImpl implements OrderRepository {

    private static final String SQL_INSERT_ITEM_TO_ORDER = "INSERT INTO order_items (order_id, item_id) VALUES(?,?)";
    private static final String SQL_INSERT_ORDER = "INSERT INTO orders (user_id, total_price) VALUES(?,?)";
    private static final String SQL_UPDATE_TOTAL_PRICE = "UPDATE orders SET total_price=? WHERE id=?";
    private static final String SQL_FIND_ITEMS_BY_ORDER = "SELECT * FROM Items INNER JOIN order_Items ON items.id = order_Items.item_id WHERE order_Items.order_id=?";
    private static final String SQL_FIND_ORDER_BY_USER = "SELECT * FROM Orders AS o WHERE o.user_id=?";

    private JdbcTemplate jdbcTemplate;

    public OrderRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void addItemToOrder(Order order, Item item) {
        jdbcTemplate.update(SQL_INSERT_ITEM_TO_ORDER, order.getId(), item.getId());
    }

    @Override
    public Optional<List<Item>> findItemsByOrder(Order order) {
        return Optional.ofNullable(jdbcTemplate.query(SQL_FIND_ITEMS_BY_ORDER, new Object[]{order.getId()}, new ItemMapper()));
    }

    @Override
    public void createOrder(User user, Double totalPrice) {
        jdbcTemplate.update(SQL_INSERT_ORDER, user.getId(), totalPrice);
    }

    @Override
    public void updateTotalPriceOrder(Order order, Double totalPrice) {
        jdbcTemplate.update(SQL_UPDATE_TOTAL_PRICE, totalPrice, order.getId());
    }

    @Override
    public Optional<Order> findOrderByUser(User user) {
        Order order;
        try {
            order = jdbcTemplate.queryForObject(SQL_FIND_ORDER_BY_USER, new Object[]{user.getId()}, new OrderMapper());
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
        return Optional.ofNullable(order);
    }

}
