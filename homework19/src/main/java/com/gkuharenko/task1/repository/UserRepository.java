package com.gkuharenko.task1.repository;

import com.gkuharenko.task1.domain.User;

import java.util.Optional;


public interface UserRepository {

    /**
     * Search a user in the database
     *
     * @param login login
     * @return {@link Optional<User>}
     */
    Optional<User> findUserByLogin(String login);

}
