package com.gkuharenko.task1.filter;

import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.service.UserService;
import com.gkuharenko.task1.service.impl.UserServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.gkuharenko.task1.filter.CheckFilter.REDIRECT_PATH_ERROR;

@WebFilter(urlPatterns = {"/items", "/order"})
public class LoginFilter implements Filter {

    public static final String USER = "authUser";
    private static final String LOGIN = "login";

    private UserService userService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        userService = (UserServiceImpl) context.getBean("userService");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpSession httpSession = ((HttpServletRequest) request).getSession();
        User user = (User) httpSession.getAttribute(USER);

        if (user == null) {
            final String login = request.getParameter(LOGIN);
            if (login == null) {
                redirect(request, response);
                return;
            }
            user = userService.getUserByLogin(request.getParameter(LOGIN));
            if (user == null) {
                redirect(request, response);
                return;
            }
            httpSession.setAttribute(USER, user);
            filterChain.doFilter(request, response);
            return;
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Redirects to the error page
     *
     * @param request  the {@link ServletRequest}
     * @param response the {@link ServletResponse}
     * @throws ServletException the ServletException
     * @throws IOException      the io exception
     */
    private void redirect(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(REDIRECT_PATH_ERROR).forward(request, response);
    }
}
