package com.gkuharenko.task1.mapper;

import com.gkuharenko.task1.domain.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_LOGIN = "login";
    private static final String COLUMN_PASSWORD = "password";

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(COLUMN_ID));
        user.setLogin(resultSet.getString(COLUMN_LOGIN));
        user.setPassword(resultSet.getString(COLUMN_PASSWORD));
        return user;
    }
}
