package com.gkuharenko.task1.mapper;

import com.gkuharenko.task1.domain.Item;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemMapper implements RowMapper<Item> {

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_PRICE = "price";

    @Override
    public Item mapRow(ResultSet resultSet, int i) throws SQLException {
        Item item = new Item();
        item.setId(resultSet.getLong(COLUMN_ID));
        item.setName(resultSet.getString(COLUMN_NAME));
        item.setPrice(resultSet.getDouble(COLUMN_PRICE));
        return item;
    }

}
