package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.repository.ItemRepository;
import com.gkuharenko.task1.service.ItemService;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class ItemServiceImpl implements ItemService {

    public static final String RESULT = "result";

    private ItemRepository itemRepository;

    public ItemServiceImpl(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public List<Item> getAllItems() {
        return itemRepository.listItems();
    }

    @Override
    public Item getItemById(String id) {
        return itemRepository.findItemById(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<Item> doTemporaryListItems(HttpSession httpSession, String id) {
        List<Item> itemList = new ArrayList<>();
        if (httpSession.getAttribute(RESULT) != null) {
            itemList = (List<Item>) httpSession.getAttribute(RESULT);
        }
        if (id != null) {
            itemList.add(getItemById(id));
            httpSession.setAttribute(RESULT, itemList);
        }
        return itemList;
    }

}
