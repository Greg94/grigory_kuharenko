package com.gkuharenko.task1.service;

import com.gkuharenko.task1.domain.User;

public interface UserService {

    /**
     * Get user by login
     *
     * @param login login
     * @return {@link User}
     */
    User getUserByLogin(String login);

}
