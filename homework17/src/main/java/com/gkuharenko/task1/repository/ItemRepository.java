package com.gkuharenko.task1.repository;

import com.gkuharenko.task1.domain.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemRepository {

    /**
     * Сreates a list of items available in the online store
     *
     * @return {@link List<Item>}
     */
    public List<Item> getDemoItems() {
        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item(1L, "Book", 5.5));
        itemList.add(new Item(2L, "Mobile Phone", 60.8));
        itemList.add(new Item(3L, "Coffee Machine", 100));
        itemList.add(new Item(4L, "Laptop", 120.45));
        itemList.add(new Item(5L, "Cup", 3.2));
        return itemList;
    }

}
