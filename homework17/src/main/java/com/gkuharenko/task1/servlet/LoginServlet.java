package com.gkuharenko.task1.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/online-shop")
public class LoginServlet extends HttpServlet {

    private static final String LOGIN_PATH = "login.jsp";

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(LOGIN_PATH).forward(req, resp);
    }

}
