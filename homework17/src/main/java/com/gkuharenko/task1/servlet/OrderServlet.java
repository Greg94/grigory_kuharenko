package com.gkuharenko.task1.servlet;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.service.ItemService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.gkuharenko.task1.service.ItemService.RESULT;

@WebServlet(urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private static final String TOTAL_PRICE = "totalPrice";
    private static final String ORDER_PATH = "WEB-INF/views/order.jsp";

    private ItemService itemService;

    @Override
    public void init() throws ServletException {
        itemService = new ItemService();
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession httpSession = req.getSession();
        List<Item> resultList = (List<Item>) httpSession.getAttribute(RESULT);
        if (resultList != null) {
            double totalPrice = itemService.doCountPrice(resultList);
            req.setAttribute(TOTAL_PRICE, totalPrice);
        }
        req.getRequestDispatcher(ORDER_PATH).forward(req, resp);
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
