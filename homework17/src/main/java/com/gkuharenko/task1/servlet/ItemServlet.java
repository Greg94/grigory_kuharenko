package com.gkuharenko.task1.servlet;

import com.gkuharenko.task1.repository.ItemRepository;
import com.gkuharenko.task1.service.ItemService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(urlPatterns = "/items")
public class ItemServlet extends HttpServlet {

    private static final String ADD_PARAMETER = "addItem";
    private static final String ITEMS_PATH = "WEB-INF/views/items.jsp";
    private static final String LIST_ITEMS = "listItems";

    private ItemRepository itemRepository;
    private ItemService itemService;

    @Override
    public void init() throws ServletException {
        itemRepository = new ItemRepository();
        itemService = new ItemService();
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession httpSession = req.getSession();
        String itemId = req.getParameter(ADD_PARAMETER);
        itemService.doListResult(httpSession, itemId);
        httpSession.setAttribute(LIST_ITEMS, itemRepository.getDemoItems());
        req.getRequestDispatcher(ITEMS_PATH).forward(req, resp);
    }
}
