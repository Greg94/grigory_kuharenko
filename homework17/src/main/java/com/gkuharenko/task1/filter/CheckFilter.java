package com.gkuharenko.task1.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.gkuharenko.task1.service.ItemService.RESULT;

@WebFilter(urlPatterns = {"/items", "/order"})
public class CheckFilter implements Filter {

    private static final String CHECK_ON = "on";
    private static final String REDIRECT_PATH = "/error";
    private static final String CHECK_PARAMETER = "checkMe";
    private static final String LOGIN_PARAMETER = "login";
    private static final String IS_CHECK = "isCheck";


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String checkMe = servletRequest.getParameter(CHECK_PARAMETER);
        HttpSession httpSession = ((HttpServletRequest) servletRequest).getSession();
        String isCheck = String.valueOf(httpSession.getAttribute(IS_CHECK));
        String login = servletRequest.getParameter(LOGIN_PARAMETER);

        if (CHECK_ON.equals(checkMe)) {
            httpSession.setAttribute(LOGIN_PARAMETER, login);
            httpSession.setAttribute(IS_CHECK, checkMe);
            httpSession.setAttribute(RESULT, null);
            filterChain.doFilter(servletRequest, servletResponse);
        } else if (CHECK_ON.equals(String.valueOf(isCheck))) {
            if (login == null) {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }
        servletRequest.getRequestDispatcher(REDIRECT_PATH).forward(servletRequest, servletResponse);
    }

}
