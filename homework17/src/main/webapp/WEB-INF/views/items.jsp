<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>

<head>
    <title>Items</title>
</head>

<body align="center">

    <h1>Hello, ${login}!</h1>
    <form action="items" method="POST">
        <p><select name="addItem" required>

                <c:forEach items="${listItems}" var="item">
                    <option value="${item.id}">
                        ${item.name} (${item.price} $)
                    </option>
                </c:forEach>

            </select></p>
        <c:set var="count" value="0" scope="page" />
        <c:forEach items="${result}" var="item">
            <c:set var="count" value="${count+1}" scope="page" />
            <p>
                <tr>
                    <td>${count})</td>
                    <td>${item.name} </td>
                    <td>${item.price}$</td>
                </tr>
            </p>
        </c:forEach>
        <p><input type="submit" value="Add Item"></p>
    </form>
    <form action="order" method="POST">
        <p><input type="submit" value="Submit"></p>
    </form>
    <p><a href="/logout">Logout</a></p>

</body>

</html>