package com.gkuharenko.task1;

import org.junit.Test;

import static org.junit.Assert.*;

public class FileSystemBuilderTest {

    @Test
    public void buildFileSystemTest() throws Exception {
        FileSystemBuilder.buildFileSystem("root/folder1/folder2/file1.txt/folderTest");
        String[] expectedLeafs = "root/folder1/folder2/file1.txt".split("/");
        int increment = 0;

        for (Tree.Branch<String> child : FileSystemBuilder.tree.root.children) {
            assertEquals(child.parent.data, expectedLeafs[increment++]);
        }

    }

}