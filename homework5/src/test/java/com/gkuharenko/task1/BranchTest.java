package com.gkuharenko.task1;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class BranchTest {

    @Test
    public void printTest() throws Exception {
        FileSystemBuilder.buildFileSystem("root/folder1/file.exe");
        String hierarchy = "root\r\n\tfolder1\r\n\t\tfile.exe\r\n";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(outputStream);
        System.setOut(ps);

        FileSystemBuilder.tree.root.print(Tree.INDENT);

        assertEquals(hierarchy, outputStream.toString());
    }

}