package com.gkuharenko.task1;

import java.util.Scanner;

public class ClientService {

    private static final String WELCOME_MSG = "Enter the path.\nEnter \"q\" to exit";
    private static final String EXIT = "q";

    private ClientService() {
    }

    /**
     * Method launches the application.
     */
    public static void runApp() {
        try (Scanner scanner = new Scanner(System.in)) {
            String path;
            while (true) {
                System.out.println(WELCOME_MSG);
                path = scanner.nextLine();
                if (path.equals(EXIT)) {
                    break;
                }
                FileSystemBuilder.buildFileSystem(path);
                FileSystemBuilder.tree.root.print(Tree.INDENT);
            }
        }
    }

}
