package com.gkuharenko.task1;

import java.util.ArrayList;
import java.util.List;

public class Tree<String> {

    public static final int INDENT = 1;
    private static final int ONE = 1;

    Branch<String> root;

    public static class Branch<String> {
        String data;
        Branch<String> parent;
        List<Branch<String>> children = new ArrayList<>();

        public Branch(final String data, final Branch<String> parent) {
            this.data = data;
            this.parent = parent;
        }

        /**
         * Method print structure.
         *
         * @param indent - indent
         */
        public void print(int indent) {
            for (int i = ONE; i < indent; i++) {
                System.out.print("\t");
            }
            System.out.println(data);
            for (Branch child : children) {
                child.print(indent + ONE);
            }
        }
    }
}
