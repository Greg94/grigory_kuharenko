package com.gkuharenko.task1;

public class Card {

    public static final int MAX_BALANCE = 1000;
    public static final int MIN_BALANCE = 0;
    private static final int DEFAULT_BALANCE = 500;

    volatile private int balance;
    private Boolean continueOperationWithCard;

    public Card() {
        this.balance = DEFAULT_BALANCE;
        this.continueOperationWithCard = Boolean.TRUE;
    }

    public int getBalance() {
        return balance;
    }


    public Boolean getContinueOperationWithCard() {
        return continueOperationWithCard;
    }

    public void setContinueOperationWithCard(Boolean continueOperationWithCard) {
        this.continueOperationWithCard = continueOperationWithCard;
    }

    /**
     * Method adds money
     *
     * @param amount - amount
     */
    public synchronized void addMoney(int amount) {
        this.balance += amount;
    }

    /**
     * Method withdraw money
     *
     * @param amount - amount
     */
    public synchronized void withdrawMoney(int amount) {
        this.balance -= amount;
    }

}
