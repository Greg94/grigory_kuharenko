package com.gkuharenko.task2;

public interface Algorithm {

    /**
     * Starts the while loop
     *
     * @param number - parameter passed to the algorithm.
     * @return result line
     */
    String loopWhile(int number);

    /**
     * Starts the do-while loop
     *
     * @param number - parameter passed to the algorithm.
     * @return result line
     */
    String loopDoWhile(int number);

    /**
     * Starts the for loop
     *
     * @param number - parameter passed to the algorithm.
     * @return result line
     */
    String loopFor(int number);

}
