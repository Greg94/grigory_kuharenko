package com.gkuharenko.task2;

/**
 * Class that implements the interface
 *
 * @see Algorithm
 */
public class Factorial implements Algorithm {

    private long factorial = 1L;

    @Override
    public String loopWhile(int number) {
        System.out.print("loopWhile ");
        int count = 1;
        while (count <= number) {
            factorial *= count;
            count++;
        }
        return String.valueOf(factorial);
    }

    @Override
    public String loopDoWhile(int number) {
        System.out.print("loopDoWhile ");
        int count = 1;
        do {
            factorial *= count;
            count++;
        } while (count <= number);
        return String.valueOf(factorial);
    }

    @Override
    public String loopFor(int number) {
        System.out.print("loopFor ");
        for (int i = 1; i <= number; ++i) {
            factorial *= i;
        }
        return String.valueOf(factorial);
    }

}
