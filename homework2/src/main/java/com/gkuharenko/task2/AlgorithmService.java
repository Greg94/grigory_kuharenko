package com.gkuharenko.task2;

import java.util.HashMap;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class AlgorithmService {

    public static final int NUMBER_OF_ARGUMENTS = 3;
    public static final int FIRST_PARAMETER_ALGORITHM = 0;
    public static final int SECOND_PARAMETER_LOOP_TYPE = 1;
    public static final int THIRD_PARAMETER_NUMBER = 2;
    public static final int NUMBER_OF_ALGORITHMS = 2;
    public static final int NUMBER_OF_CYCLES = 3;
    public static final int NUMBER_ZERO = 0;
    public static final String FIBONACCI = "1";
    public static final String FACTORIAL = "2";
    public static final String LOOP_TYPE_WHILE = "1";
    public static final String LOOP_TYPE_DO_WHILE = "2";
    public static final String LOOP_TYPE_FOR = "3";
    public static final String ERROR_NOT_ENOUGH_ARGUMENTS = "Please choice 3 arguments!";
    public static final String ERROR_INPUT = "Warning! Check the arguments.";
    public static final String NUMBER_ENTRY_ERROR = "Only positive numbers!";
    public static final String LOOP_TYPE_ERROR = "Please, choice loop type 1-3!";
    public static final String ALGORITHM_ERROR = "Please, choice algorithm 1 or 2!";

    private String algorithmId;
    private String loopType;
    private int number;

    private HashMap<String, BiFunction<Algorithm, Integer, String>> loopTypeMap
            = new HashMap<String, BiFunction<Algorithm, Integer, String>>() {
        {
            put(LOOP_TYPE_WHILE, Algorithm::loopWhile);
            put(LOOP_TYPE_DO_WHILE, Algorithm::loopDoWhile);
            put(LOOP_TYPE_FOR, Algorithm::loopFor);
        }
    };
    private HashMap<String, Supplier<String>> algorithmMap = new HashMap<String, Supplier<String>>() {
        {
            put(FIBONACCI, () -> loopTypeMap.get(loopType).apply(new Fibonacci(), number));
            put(FACTORIAL, () -> loopTypeMap.get(loopType).apply(new Factorial(), number));
        }
    };

    /**
     * Gets the result
     *
     * @param args - settable parameters
     * @return - string result
     */
    public String doResult(String[] args) {
        String result = "";
        if (isValidate(args)) {
            result = algorithmMap.get(algorithmId).get();
            printResult(result);
            return result;
        }
        return result;
    }

    /**
     * Checks if there are enough entered parameters
     *
     * @param args - settable parameters
     * @return true if the number of parameters not less than 3, else false
     */
    private boolean isEnough(String[] args) {
        if (args.length >= NUMBER_OF_ARGUMENTS) {
            return true;
        } else {
            System.err.println(ERROR_NOT_ENOUGH_ARGUMENTS);
            return false;
        }
    }

    /**
     * Validation check
     *
     * @param args - settable parameters
     * @return true if all parameters are validate
     */
    private boolean isValidate(String[] args) {
        if (isNumerical(args)) {
            if (Integer.parseInt(args[FIRST_PARAMETER_ALGORITHM]) <= NUMBER_OF_ALGORITHMS
                    && Integer.parseInt(args[FIRST_PARAMETER_ALGORITHM]) > NUMBER_ZERO) {
                algorithmId = args[FIRST_PARAMETER_ALGORITHM];
                if (Integer.parseInt(args[SECOND_PARAMETER_LOOP_TYPE]) <= NUMBER_OF_CYCLES
                        && Integer.parseInt(args[SECOND_PARAMETER_LOOP_TYPE]) > NUMBER_ZERO) {
                    loopType = args[SECOND_PARAMETER_LOOP_TYPE];
                    if (Integer.parseInt(args[THIRD_PARAMETER_NUMBER]) >= NUMBER_ZERO) {
                        number = Integer.parseInt(args[THIRD_PARAMETER_NUMBER]);
                        return true;
                    } else {
                        System.err.println(NUMBER_ENTRY_ERROR);
                        return false;
                    }
                } else {
                    System.err.println(LOOP_TYPE_ERROR);
                    return false;
                }
            } else {
                System.err.println(ALGORITHM_ERROR);
                return false;
            }
        }
        return false;
    }

    /**
     * Checks if all parameters are digits
     *
     * @param args - settable parameters
     * @return true if all entered parameters are digits, else false
     */
    private boolean isNumerical(String[] args) {
        if (isEnough(args)) {
            try {
                Integer.parseInt(args[FIRST_PARAMETER_ALGORITHM]);
                Integer.parseInt(args[SECOND_PARAMETER_LOOP_TYPE]);
                Integer.parseInt(args[THIRD_PARAMETER_NUMBER]);
            } catch (NumberFormatException nfe) {
                System.err.println(ERROR_INPUT);
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Prints the result
     *
     * @param result - result for output
     */
    private void printResult(String result) {
        System.out.println("Result: " + result);
    }

}