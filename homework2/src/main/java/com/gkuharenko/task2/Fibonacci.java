package com.gkuharenko.task2;

/**
 * Class that implements the interface
 *
 * @see Algorithm
 */
public class Fibonacci implements Algorithm {
    private int previousNumber = 0;
    private int nextNumber = 1;

    @Override
    public String loopWhile(int number) {
        System.out.print("loopWhile ");
        int count = 1;
        StringBuilder fibonacciNumbers = new StringBuilder();
        while (count <= number) {
            fibonacciNumbers.append(previousNumber).append(" ");
            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
            count++;
        }
        return fibonacciNumbers.toString();
    }

    @Override
    public String loopDoWhile(int number) {
        System.out.print("loopDoWhile ");
        int count = 1;
        StringBuilder fibonacciNumbers = new StringBuilder();
        do {
            fibonacciNumbers.append(previousNumber).append(" ");
            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
            count++;
        } while (count <= number);
        return fibonacciNumbers.toString();
    }

    @Override
    public String loopFor(int number) {
        System.out.print("loopFor ");
        StringBuilder fibonacciNumbers = new StringBuilder();
        for (int i = 1; i <= number; ++i) {
            fibonacciNumbers.append(previousNumber).append(" ");
            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
        }
        return fibonacciNumbers.toString();
    }
}
