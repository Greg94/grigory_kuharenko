package com.gkuharenko.task1;

import static java.lang.Math.PI;

public final class FormulaUtill {
    public static final int NUMBER_OF_ARGUMENTS = 4;
    public static final int FIRST_PARAMETER_A = 0;
    public static final int SECOND_PARAMETER_P = 1;
    public static final int THIRD_PARAMETER_M1 = 2;
    public static final int FOURTH_PARAMETER_M2 = 3;
    public static final String ERROR_NOT_ENOUGH_ARGUMENTS = "Please choice 4 arguments!";
    public static final String ERROR_INPUT = "Warning! Check the arguments.";

    private static int a;
    private static int p;
    private static double m1;
    private static double m2;
    private static double result;

    private FormulaUtill() {
    }

    /**
     * Gets the result
     *
     * @param args - settable parameters
     * @return - returns the result by the formula
     */
    public static double doResult(String[] args) {
        if (isNumerical(args)) {
            result = 4 * Math.pow(PI, 2) * Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2));
            printResult(result);
        }
        return result;
    }

    /**
     * Checks if there are enough entered parameters
     *
     * @param args - settable parameters
     * @return true if the number of parameters not less than 4, else false
     */
    private static boolean isEnough(String[] args) {
        if (args.length >= NUMBER_OF_ARGUMENTS) {
            return true;
        } else {
            System.err.println(ERROR_NOT_ENOUGH_ARGUMENTS);
            return false;
        }
    }

    /**
     * Checks if all parameters are digits
     *
     * @param args - settable parameters
     * @return true if all entered parameters are digits, else false
     */
    private static boolean isNumerical(String[] args) {
        if (isEnough(args)) {
            try {
                a = Integer.parseInt(args[FIRST_PARAMETER_A]);
                p = Integer.parseInt(args[SECOND_PARAMETER_P]);
                m1 = Double.parseDouble(args[THIRD_PARAMETER_M1]);
                m2 = Double.parseDouble(args[FOURTH_PARAMETER_M2]);
            } catch (IllegalArgumentException nfe) {
                System.err.println(ERROR_INPUT);
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Prints the result
     *
     * @param result - result for output
     */
    private static void printResult(double result) {
        System.out.println("Result: " + result);
    }

}
