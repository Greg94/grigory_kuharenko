package com.gkuharenko.task1;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static java.lang.Double.NaN;
import static java.lang.Double.POSITIVE_INFINITY;


@RunWith(Parameterized.class)
public class FormulaUtillTest {

    private String[] args;
    private double expectedResult;
    private FormulaUtill formulaUtill;

    public FormulaUtillTest(String[] args, double expectedResult) {
        this.args = args;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {new String[]{"3", "2", "3.5", "5.6"}, 29.283441629605786},
                {new String[]{"1", "1", "1", "1"}, 19.739208802178716},
                {new String[]{"3", "2", "3.5", "5.6", "2", "3"}, 29.283441629605786},
                {new String[]{"1", "0", "2.9", "1.4"}, POSITIVE_INFINITY},
                {new String[]{"0", "0", "1.5", "2.3"}, NaN},
                {new String[]{"3", "2", "3,5"}, NaN},
                {new String[]{"s", "l", "k", "t"}, NaN}
        });
    }

    @Test
    public void testDoResult() {
        Assert.assertEquals(expectedResult, FormulaUtill.doResult(args), 0.001);
    }

}