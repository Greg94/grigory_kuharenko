package com.gkuharenko.task2;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;


@RunWith(Parameterized.class)
public class AlgorithmServiceTest {

    private String[] args;
    private String expectedResult;
    private AlgorithmService algorithmService;

    public AlgorithmServiceTest(String[] args, String expectedResult) {
        this.args = args;
        this.expectedResult = expectedResult;
        this.algorithmService = new AlgorithmService();
    }

    @Parameterized.Parameters
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {new String[]{"1", "1", "5"}, "0 1 1 2 3 "},
                {new String[]{"2", "2", "5"}, "120"},
                {new String[]{"3", "3", "5"}, ""},
                {new String[]{"1", "3", "7"}, "0 1 1 2 3 5 8 "},
                {new String[]{"0", "2", "5"}, ""},
                {new String[]{"1", "5", "5"}, ""},
                {new String[]{"2", "2", "0"}, "1"},
                {new String[]{"2", "2"}, ""},
                {new String[]{"1", "2", "f"}, ""},
                {new String[]{"1", "3", "0"}, ""},
                {new String[]{"1", "2", "-3"}, ""}
        });
    }

    @Test
    public void testDoResult() {
        Assert.assertEquals(expectedResult, algorithmService.doResult(args));
    }
}