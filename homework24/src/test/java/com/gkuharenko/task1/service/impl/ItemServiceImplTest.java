package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.repository.ItemRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ItemServiceImplTest {

    @Mock
    private ItemRepository itemRepository;
    @InjectMocks
    private ItemServiceImpl itemService;

    @Test
    public void testGetItemById() {
        //given
        Long id = 1L;
        Item testItem = new Item("Book", BigDecimal.valueOf(5.5));
        when(itemRepository.findItemById(id)).thenReturn(Optional.of(testItem));

        //when
        Optional<Item> itemResult = Optional.ofNullable(itemService.getItemById(id));

        //then
        Assert.assertTrue(itemResult.isPresent());
        Assert.assertEquals(testItem, itemResult.get());

        verify(itemRepository).findItemById(id);
    }

    @Test(expected = RuntimeException.class)
    public void testGetItemByIdFails() {
        //given
        Long id = 99L;
        when(itemRepository.findItemById(id)).thenThrow(RuntimeException.class);

        //when
        itemService.getItemById(id);

        //then
        verify(itemRepository).findItemById(id);
    }


}