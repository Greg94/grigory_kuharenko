package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.entity.Order;
import com.gkuharenko.task1.entity.Role;
import com.gkuharenko.task1.entity.User;
import com.gkuharenko.task1.repository.OrderRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

    private List<Item> itemList;
    private Order testOrder;
    private User user;

    @Before
    public void setUp() throws Exception {
        itemList = new ArrayList<>();
        itemList.add(new Item("Book", BigDecimal.valueOf(5.5)));
        itemList.add(new Item("Laptop", BigDecimal.valueOf(120.45)));
        itemList.add(new Item("Coffee Machine", BigDecimal.valueOf(100)));

        user = new User("test", "test", Role.ADMIN);

        testOrder = new Order(user, BigDecimal.valueOf(100));
        testOrder.setItemList(itemList);
    }

    @Mock
    private OrderRepository orderRepository;
    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void testGetItemsByOrder() {
        //given
        when(orderRepository.findItemsByOrder(testOrder)).thenReturn(Optional.of(itemList));

        //when
        Optional<List<Item>> itemsResult = Optional.ofNullable(orderService.getItemsByOrder(testOrder));

        //then
        Assert.assertTrue(itemsResult.isPresent());
        Assert.assertEquals(itemList, itemsResult.get());

        verify(orderRepository).findItemsByOrder(testOrder);
    }

    @Test(expected = RuntimeException.class)
    public void testGetItemsByOrderFails() {
        //given
        Order testOrder = new Order(new User(), BigDecimal.valueOf(100));
        when(orderRepository.findItemsByOrder(testOrder)).thenThrow(RuntimeException.class);

        //when
        orderService.getItemsByOrder(testOrder);

        //then
        verify(orderRepository).findItemsByOrder(testOrder);
    }

    @Test
    public void testGetOrderByUser() {
        //given
        when(orderRepository.findOrderByUser(user)).thenReturn(Optional.of(testOrder));

        //when
        Optional<Order> orderResult = Optional.ofNullable(orderService.getOrderByUser(user));

        //then
        Assert.assertTrue(orderResult.isPresent());
        Assert.assertEquals(testOrder, orderResult.get());

        verify(orderRepository).findOrderByUser(user);
    }

    @Test(expected = NullPointerException.class)
    public void testGetOrderByUserFails() {
        //given
        User user = new User("test", "test", Role.ADMIN);
        when(orderRepository.findOrderByUser(user)).thenReturn(null);

        //when
        orderService.getOrderByUser(user);

        //then
        verify(orderRepository).findOrderByUser(user);
    }

}