package com.gkuharenko.task1.service;

import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.entity.Order;
import com.gkuharenko.task1.entity.User;

import java.util.List;


public interface OrderService {

    /**
     * Create order
     *
     * @param user  user
     * @param items items
     * @return {@link Order}
     */
    Order createOrder(User user, List<Item> items);

    /**
     * Update order
     *
     * @param order order
     * @param items items
     * @return {@link Order}
     */
    Order updateOrder(Order order, List<Item> items);

    /**
     * Get items by order
     *
     * @param order order
     * @return {@link List<Item>}
     */
    List<Item> getItemsByOrder(Order order);

    /**
     * Get order by user
     *
     * @param user user
     * @return {@link Order}
     */
    Order getOrderByUser(User user);

    /**
     * Delete order by user
     *
     * @param order
     */
    void deleteOrder(Order order);

}
