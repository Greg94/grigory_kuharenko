package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.entity.Order;
import com.gkuharenko.task1.entity.User;
import com.gkuharenko.task1.exception.OrderNotFoundException;
import com.gkuharenko.task1.repository.OrderRepository;
import com.gkuharenko.task1.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order createOrder(User user, List<Item> items) {
        Order newOrder = new Order();
        newOrder.setUser(user);
        newOrder.getItemList().addAll(items);
        newOrder.setTotalPrice(doCountPrice(newOrder));
        return orderRepository.saveOrder(newOrder);
    }

    @Override
    public Order updateOrder(Order order, List<Item> items) {
        order.setUser(order.getUser());
        List<Item> itemList = getItemsByOrder(order);
        order.setItemList(itemList);
        order.getItemList().addAll(items);
        order.setTotalPrice(doCountPrice(order));
        return orderRepository.updateOrder(order);
    }

    @Override
    public void deleteOrder(Order order) {
        orderRepository.deleteOrder(order);
    }

    private BigDecimal doCountPrice(Order order) {
        return BigDecimal.valueOf(order.getItemList().stream().mapToDouble(value -> value.getPrice().doubleValue()).sum());
    }

    @Override
    public List<Item> getItemsByOrder(Order order) {
        return orderRepository.findItemsByOrder(order).orElseThrow(OrderNotFoundException::new);
    }

    @Override
    public Order getOrderByUser(User user) {
        return orderRepository.findOrderByUser(user).orElseThrow(OrderNotFoundException::new);
    }
}
