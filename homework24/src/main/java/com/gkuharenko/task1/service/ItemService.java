package com.gkuharenko.task1.service;

import com.gkuharenko.task1.entity.Item;

import java.util.List;

public interface ItemService {

    /**
     * Get all items
     *
     * @return {@link List<Item>}
     */
    List<Item> getAllItems();

    /**
     * Get item by id
     *
     * @param id item id
     * @return {@link Item}
     */
    Item getItemById(Long id);

}
