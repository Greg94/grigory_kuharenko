package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.entity.User;
import com.gkuharenko.task1.exception.UserNotFoundException;
import com.gkuharenko.task1.repository.UserRepository;
import com.gkuharenko.task1.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUserByLogin(String login) {
        return userRepository.findUserByLogin(login).orElseThrow(UserNotFoundException::new);
    }

}
