package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.exception.ItemNotFoundException;
import com.gkuharenko.task1.repository.ItemRepository;
import com.gkuharenko.task1.service.ItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    public ItemServiceImpl(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public List<Item> getAllItems() {
        return itemRepository.listItems();
    }

    @Override
    public Item getItemById(Long id) {
        return itemRepository.findItemById(id).orElseThrow(ItemNotFoundException::new);
    }

}
