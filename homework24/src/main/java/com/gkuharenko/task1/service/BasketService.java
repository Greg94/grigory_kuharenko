package com.gkuharenko.task1.service;

import javax.servlet.http.HttpSession;

public interface BasketService {

    /**
     * Add items to basket. Selected items are stored in session.
     *
     * @param httpSession - httpSession
     * @param id          - items id
     */
    void addItemsToBasket(HttpSession httpSession, Long id);

}
