package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.dto.ItemDto;
import com.gkuharenko.task1.mapper.ItemMapper;
import com.gkuharenko.task1.service.BasketService;
import com.gkuharenko.task1.service.ItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BasketServiceImpl implements BasketService {

    public static final String RESULT = "result";
    private final ItemService itemService;
    private final ItemMapper itemMapper;

    public BasketServiceImpl(ItemService itemService, ItemMapper itemMapper) {
        this.itemService = itemService;
        this.itemMapper = itemMapper;
    }

    @Override
    public void addItemsToBasket(HttpSession httpSession, Long id) {
        List<ItemDto> itemList = new ArrayList<>();
        if (httpSession.getAttribute(RESULT) != null) {
            itemList = (List<ItemDto>) httpSession.getAttribute(RESULT);
        }
        if (id != null) {
            itemList.add(itemMapper.toDto(itemService.getItemById(id)));
            httpSession.setAttribute(RESULT, itemList);
        }
    }
}
