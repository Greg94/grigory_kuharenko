package com.gkuharenko.task1.controller;

import com.gkuharenko.task1.exception.ItemNotFoundException;
import com.gkuharenko.task1.service.BasketService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/baskets")
public class BasketResource {

    private static final String ITEM_NOT_FOUND = "Item Not Found";
    private final BasketService basketService;

    public BasketResource(BasketService basketService) {
        this.basketService = basketService;
    }

    @PutMapping(value = "/items/{id}")
    public ResponseEntity addItemToBasket(@PathVariable Long id, HttpSession httpSession) {
        try {
            basketService.addItemsToBasket(httpSession, id);
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (ItemNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ITEM_NOT_FOUND, e);
        }
    }

}
