package com.gkuharenko.task1.controller;

import com.gkuharenko.task1.dto.UserDto;
import com.gkuharenko.task1.entity.User;
import com.gkuharenko.task1.mapper.UserMapper;
import com.gkuharenko.task1.service.UserService;
import com.gkuharenko.task1.service.impl.CustomUserDetailsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserResource {

    private final CustomUserDetailsService userDetailsService;
    private final UserService userService;
    private final UserMapper userMapper;

    public UserResource(CustomUserDetailsService userDetailsService, UserService userService, UserMapper userMapper) {
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping(value = "/current-user")
    public ResponseEntity<UserDto> getCurrentUser() {
        String userLogin = userDetailsService.getPrincipal();
        User currentUser = userService.getUserByLogin(userLogin);
        return ResponseEntity.ok(userMapper.toDto(currentUser));
    }

}
