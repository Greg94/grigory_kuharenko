package com.gkuharenko.task1.controller;

import com.gkuharenko.task1.dto.ItemDto;
import com.gkuharenko.task1.exception.ItemNotFoundException;
import com.gkuharenko.task1.mapper.ItemMapper;
import com.gkuharenko.task1.service.ItemService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/items")
public class ItemResource {

    private static final String ITEM_NOT_FOUND = "Item Not Found";
    private final ItemService itemService;
    private final ItemMapper itemMapper;

    public ItemResource(ItemService itemService, ItemMapper itemMapper) {
        this.itemService = itemService;
        this.itemMapper = itemMapper;
    }

    @GetMapping()
    public ResponseEntity<List<ItemDto>> getListCustomItems() {
        return ResponseEntity.ok(itemService.getAllItems().stream().map(itemMapper::toDto).collect(Collectors.toList()));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ItemDto> getItemById(@PathVariable Long id) {
        try {
            ItemDto itemDto = itemMapper.toDto(itemService.getItemById(id));
            return ResponseEntity.ok(itemDto);
        } catch (ItemNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ITEM_NOT_FOUND, e);
        }
    }

}
