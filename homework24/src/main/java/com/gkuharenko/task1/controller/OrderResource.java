package com.gkuharenko.task1.controller;

import com.gkuharenko.task1.dto.ItemDto;
import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.entity.Order;
import com.gkuharenko.task1.entity.User;
import com.gkuharenko.task1.exception.OrderNotFoundException;
import com.gkuharenko.task1.mapper.ItemMapper;
import com.gkuharenko.task1.mapper.OrderMapper;
import com.gkuharenko.task1.service.OrderService;
import com.gkuharenko.task1.service.UserService;
import com.gkuharenko.task1.service.impl.CustomUserDetailsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static com.gkuharenko.task1.service.impl.BasketServiceImpl.RESULT;

@RestController
@RequestMapping("/orders")
public class OrderResource {

    private static final String ORDER_IS_CREATED = "Order is created!";
    private static final String ORDER_NOT_FOUND = "Order Not Found";
    private final OrderService orderService;
    private final UserService userService;
    private final CustomUserDetailsService userDetailsService;
    private final ItemMapper itemMapper;
    private final OrderMapper orderMapper;

    public OrderResource(OrderService orderService,
                         UserService userService,
                         CustomUserDetailsService userDetailsService,
                         ItemMapper itemMapper,
                         OrderMapper orderMapper) {
        this.orderService = orderService;
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.itemMapper = itemMapper;
        this.orderMapper = orderMapper;
    }

    @PostMapping(value = "/items")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> createOrder(@SessionAttribute(RESULT) List<ItemDto> itemDtoList, HttpSession httpSession) {
        try {
            orderService.getOrderByUser(getCurrentUser());
            throw new ResponseStatusException(HttpStatus.CONFLICT, ORDER_IS_CREATED);
        } catch (OrderNotFoundException e) {
            List<Item> items = itemDtoList.stream().map(itemMapper::toEntity).collect(Collectors.toList());
            Order order = orderService.createOrder(getCurrentUser(), items);
            final URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .buildAndExpand(orderMapper.toDto(order))
                    .toUri();
            httpSession.removeAttribute(RESULT);
            return ResponseEntity.created(uri).build();
        }
    }

    @PutMapping(value = "/items")
    public ResponseEntity<Void> updateOrder(@SessionAttribute(RESULT) List<ItemDto> itemDtoList, HttpServletRequest servletRequest) throws URISyntaxException {
        HttpSession httpSession = servletRequest.getSession();
        List<Item> items = itemDtoList.stream().map(itemMapper::toEntity).collect(Collectors.toList());
        orderService.updateOrder(getOrder(), items);
        httpSession.removeAttribute(RESULT);
        return ResponseEntity.noContent().location(new URI(servletRequest.getRequestURI())).build();
    }

    @GetMapping(value = "/items")
    public ResponseEntity<List<ItemDto>> getOrderItems() {
        List<ItemDto> itemDtoList = orderService.getItemsByOrder(getOrder()).stream().map(itemMapper::toDto).collect(Collectors.toList());
        return ResponseEntity.ok(itemDtoList);
    }

    @DeleteMapping()
    public ResponseEntity deleteOrder() {
        orderService.deleteOrder(getOrder());
        return ResponseEntity.noContent().build();
    }

    private User getCurrentUser() {
        return userService.getUserByLogin(userDetailsService.getPrincipal());
    }

    private Order getOrder() {
        try {
            return orderService.getOrderByUser(getCurrentUser());
        } catch (OrderNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ORDER_NOT_FOUND, e);
        }
    }

}
