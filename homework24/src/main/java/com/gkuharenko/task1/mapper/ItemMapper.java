package com.gkuharenko.task1.mapper;

import com.gkuharenko.task1.dto.ItemDto;
import com.gkuharenko.task1.entity.Item;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ItemMapper {

    /**
     * Converts entity to dto
     *
     * @param item - {@link Item}
     * @return {@link ItemDto}
     */
    public ItemDto toDto(Item item) {
        return new ItemDto(item.getTitle(), item.getPrice().toString());
    }

    /**
     * Converts dto to entity
     *
     * @param itemDto - {@link ItemDto}
     * @return {@link Item}
     */
    public Item toEntity(ItemDto itemDto) {
        return new Item(itemDto.getTitle(), new BigDecimal(itemDto.getPrice()));
    }

}
