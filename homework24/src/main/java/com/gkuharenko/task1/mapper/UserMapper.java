package com.gkuharenko.task1.mapper;

import com.gkuharenko.task1.dto.UserDto;
import com.gkuharenko.task1.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    /**
     * Converts entity to dto
     *
     * @param user - {@link User}
     * @return {@link UserDto}
     */
    public UserDto toDto(User user) {
        return new UserDto(user.getLogin(), user.getRole().toString());
    }

}
