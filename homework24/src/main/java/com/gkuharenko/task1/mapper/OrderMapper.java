package com.gkuharenko.task1.mapper;

import com.gkuharenko.task1.dto.ItemDto;
import com.gkuharenko.task1.dto.OrderDto;
import com.gkuharenko.task1.entity.Order;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderMapper {

    private final ItemMapper itemMapper;

    public OrderMapper(ItemMapper itemMapper) {
        this.itemMapper = itemMapper;
    }

    /**
     * Converts entity to dto
     *
     * @param order - {@link Order}
     * @return {@link OrderDto}
     */
    public OrderDto toDto(Order order) {
        List<ItemDto> itemDtos = order.getItemList().stream().map(itemMapper::toDto).collect(Collectors.toList());
        return new OrderDto(order.getUser().getLogin(), itemDtos, order.getTotalPrice().toString());
    }

}
