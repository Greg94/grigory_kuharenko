package com.gkuharenko.task1.dto;

import java.util.List;

public class OrderDto {

    private String user;
    private List<ItemDto> itemDtoList;
    private String total_price;

    public OrderDto(String user, List<ItemDto> itemDtoList, String total_price) {
        this.user = user;
        this.itemDtoList = itemDtoList;
        this.total_price = total_price;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<ItemDto> getItemDtoList() {
        return itemDtoList;
    }

    public void setItemDtoList(List<ItemDto> itemDtoList) {
        this.itemDtoList = itemDtoList;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }
}
