package com.gkuharenko.task1.repository;


import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.entity.Order;
import com.gkuharenko.task1.entity.User;

import java.util.List;
import java.util.Optional;

public interface OrderRepository {

    /**
     * Search products in the order database
     *
     * @param order order
     * @return {@link Optional<List>}
     */
    Optional<List<Item>> findItemsByOrder(Order order);

    /**
     * Search a user order in the database
     *
     * @param user user
     * @return {@link Optional<Order>}
     */
    Optional<Order> findOrderByUser(User user);

    /**
     * Save order
     *
     * @param order order
     * @return {@link Order}
     */
    Order saveOrder(Order order);

    /**
     * Update order
     *
     * @param order order
     * @return {@link Order}
     */
    Order updateOrder(Order order);

    /**
     * Delete order
     *
     * @param order order
     * @return {@link Order}
     */
    void deleteOrder(Order order);

}
