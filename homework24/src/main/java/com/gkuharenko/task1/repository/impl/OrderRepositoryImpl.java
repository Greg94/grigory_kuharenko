package com.gkuharenko.task1.repository.impl;

import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.entity.Order;
import com.gkuharenko.task1.entity.User;
import com.gkuharenko.task1.repository.AbstractRepository;
import com.gkuharenko.task1.repository.OrderRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class OrderRepositoryImpl extends AbstractRepository<Long, Order> implements OrderRepository {

    private static final String HQL_FIND_ITEMS_BY_ORDER = "SELECT i FROM Order o JOIN o.itemList i WHERE o.id=:idOrder";
    private static final String HQL_FIND_ORDER_BY_USER = "FROM Order o WHERE o.user=:user";
    private static final String PARAMETER_ID_ORDER = "idOrder";
    private static final String PARAMETER_USER = "user";

    @Override
    public Optional<List<Item>> findItemsByOrder(Order order) {
        try {
            return Optional.ofNullable(getEntityManager()
                    .createQuery(HQL_FIND_ITEMS_BY_ORDER, Item.class)
                    .setParameter(PARAMETER_ID_ORDER, order.getId())
                    .getResultList());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Order> findOrderByUser(User user) {
        try {
            return Optional.ofNullable(getEntityManager()
                    .createQuery(HQL_FIND_ORDER_BY_USER, Order.class)
                    .setParameter(PARAMETER_USER, user)
                    .getSingleResult());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public Order saveOrder(Order order) {
        return save(order);
    }

    @Override
    public Order updateOrder(Order order) {
        return update(order);
    }

    @Override
    public void deleteOrder(Order order) {
        delete(order);
    }

}
