package com.gkuharenko.task1.repository.impl;

import com.gkuharenko.task1.entity.Item;
import com.gkuharenko.task1.repository.AbstractRepository;
import com.gkuharenko.task1.repository.ItemRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class ItemRepositoryImpl extends AbstractRepository<Long, Item> implements ItemRepository {

    @Override
    public List<Item> listItems() {
        return findAll();
    }

    @Override
    public Optional<Item> findItemById(Long id) {
        return Optional.ofNullable(findOne(id));
    }
}
