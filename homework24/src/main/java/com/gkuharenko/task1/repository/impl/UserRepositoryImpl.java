package com.gkuharenko.task1.repository.impl;

import com.gkuharenko.task1.entity.User;
import com.gkuharenko.task1.repository.AbstractRepository;
import com.gkuharenko.task1.repository.UserRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractRepository<Long, User> implements UserRepository {

    private static final String HQL_SELECT_BY_LOGIN = "FROM User WHERE login=:login";
    private static final String PARAMETER_LOGIN = "login";

    @Override
    public Optional<User> findUserByLogin(String login) {
        return Optional.ofNullable(getEntityManager()
                .createQuery(HQL_SELECT_BY_LOGIN, User.class)
                .setParameter(PARAMETER_LOGIN, login)
                .getSingleResult());
    }

}
