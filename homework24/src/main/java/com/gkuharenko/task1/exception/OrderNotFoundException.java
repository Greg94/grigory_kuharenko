package com.gkuharenko.task1.exception;

public class OrderNotFoundException extends RuntimeException {

    private static final String USER_NOT_FOUND = "Order not found";

    private final String message;

    public OrderNotFoundException(String message) {
        this.message = message;
    }

    public OrderNotFoundException() {
        this.message = USER_NOT_FOUND;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
