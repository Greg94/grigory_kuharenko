package com.gkuharenko.task1.exception;

public class ItemNotFoundException extends RuntimeException {

    private static final String USER_NOT_FOUND = "Item not found";

    private final String message;

    public ItemNotFoundException(String message) {
        this.message = message;
    }

    public ItemNotFoundException() {
        this.message = USER_NOT_FOUND;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
