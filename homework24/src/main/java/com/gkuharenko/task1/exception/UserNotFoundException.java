package com.gkuharenko.task1.exception;

public class UserNotFoundException extends RuntimeException {

    private static final String USER_NOT_FOUND = "User not found";

    private final String message;

    public UserNotFoundException(String message) {
        this.message = message;
    }

    public UserNotFoundException() {
        this.message = USER_NOT_FOUND;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
