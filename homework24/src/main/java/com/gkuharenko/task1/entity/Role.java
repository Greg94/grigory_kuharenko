package com.gkuharenko.task1.entity;

public enum Role {
    ADMIN,
    USER,
    ANONYMOUS
}
