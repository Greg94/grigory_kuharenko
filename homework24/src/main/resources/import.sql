INSERT INTO Users(login, password, role) VALUES ('admin', '$2a$04$z64lCcmodsQ8/o0Ls0RCluYhhWZZcz3CCwL4XdaHrFtoSCCH6t5lq','ADMIN');
INSERT INTO Users(login, password, role) VALUES ('user', '$2y$12$jYOqxcyDijtENCAtCO/aX.TumHlWIbzM.O3c2BQkw/KQQa0bEicAi','USER');

INSERT INTO Items(title, price) VALUES ('Book', 5.5);
INSERT INTO Items(title, price) VALUES ('Mobile Phone', 60.8);
INSERT INTO Items(title, price) VALUES ('Coffee Machine', 100);
INSERT INTO Items(title, price) VALUES ('Laptop', 120.45);
INSERT INTO Items(title, price) VALUES ('Cup', 3.2);

