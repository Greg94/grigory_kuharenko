package com.gkuharenko.task1_2.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Article {

    @JsonProperty("id")
    private long id;
    @JsonProperty("userId")
    private long userId;
    @JsonProperty("title")
    private String title;
    @JsonProperty("body")
    private String message;

    public Article(){}

    public Article(long id, long userId, String title, String message) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.message = message;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return id == article.id &&
                userId == article.userId &&
                Objects.equals(title, article.title) &&
                Objects.equals(message, article.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, title, message);
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
