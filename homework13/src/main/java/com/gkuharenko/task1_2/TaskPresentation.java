package com.gkuharenko.task1_2;

import com.gkuharenko.task1_2.domain.Article;
import com.gkuharenko.task1_2.service.api.ClientService;
import com.gkuharenko.task1_2.service.impl.HttpClientImpl;
import com.gkuharenko.task1_2.service.impl.UrlConnectionImpl;

import java.io.IOException;
import java.util.Scanner;

public class TaskPresentation {

    private static final String ARTICLE_GET_MSG = "Article[%s]: User [%s] Title [\"%s\"] Message [\"%s\"]\n";
    private static final String ARTICLE_POST_MSG = "Article[%s] has been created: User [%s] Title [\"%s\"] Message [\"%s\"]\n";
    private static final String ID_ERROR = "There is no article for the specified id.";
    private static final String CREATE_ERROR = "Failed to create article.";
    private static final String FIRST_ARGUMENT = "1";
    private static final String SECOND_ARGUMENT = "2";
    private static final String EXIT = "0";
    private static final String ARTICLE_ID = "article id: ";

    private ClientService clientService;

    /**
     * Method launches the application
     */
    void startPresentation() {
        try (Scanner scanner = new Scanner(System.in)) {
            doChoiceClientService(scanner);
        }
    }

    /**
     * The method provides a choice of service.
     *
     * @param scanner - scanner
     */
    private void doChoiceClientService(Scanner scanner) {
        while (true) {
            showMenu();
            String input = scanner.next();
            if (input.equals(FIRST_ARGUMENT)) {
                clientService = new UrlConnectionImpl();
                choiceMethod(scanner);
            } else if (input.equals(SECOND_ARGUMENT)) {
                clientService = new HttpClientImpl();
                choiceMethod(scanner);
            } else if (input.equals(EXIT)) {
                break;
            }
        }
    }

    /**
     * Method provides get or post selection
     *
     * @param scanner - scanner
     */
    private void choiceMethod(Scanner scanner) {
        while (true) {
            showMethods();
            String input = scanner.next();
            if (input.equals(FIRST_ARGUMENT)) {
                doGet(scanner);
            } else if (input.equals(SECOND_ARGUMENT)) {
                doPost();
            } else if (input.equals(EXIT)) {
                break;
            }
        }
    }

    /**
     * Calls the get method
     *
     * @param scanner - scanner
     */
    private void doGet(Scanner scanner) {
        System.out.print(ARTICLE_ID);
        String id = scanner.next();
        try {
            printGet(clientService.getArticle(id));
        } catch (IOException e) {
            System.err.println(ID_ERROR);
        }
    }

    /**
     * Calls the post method
     */
    private void doPost() {
        try {
            printPost(clientService.postArticle());
        } catch (IOException e) {
            System.err.println(CREATE_ERROR);
        }
    }

    /**
     * Method prints the received article
     *
     * @param article - article
     */
    private void printGet(Article article) {
        System.out.printf(ARTICLE_GET_MSG, article.getId(), article.getUserId(), article.getTitle(), article.getMessage());
    }

    /**
     * Method prints the created article
     *
     * @param newArticle - new article
     */
    private void printPost(Article newArticle) {
        System.out.printf(ARTICLE_POST_MSG, newArticle.getId(), newArticle.getUserId(), newArticle.getTitle(), newArticle.getMessage());
    }


    /**
     * Method prints menu
     */
    private void showMenu() {
        System.out.println("########MENU########");
        System.out.println("1 - Use URLConnection.");
        System.out.println("2 - Use HTTPClient.");
        System.out.println("0 - Exit from application.");
    }

    /**
     * Print method selection
     */
    private void showMethods() {
        System.out.println("**********Method selection**********");
        System.out.println("1 - GET");
        System.out.println("2 - POST");
        System.out.println("0 - Return to previous menu");
        System.out.println("************************************");
    }
}
