package com.gkuharenko.task1_2.exception;

import java.io.IOException;

public class NotFoundException extends IOException {
    public NotFoundException() {
        super(" 404 Not found.");
    }
}
