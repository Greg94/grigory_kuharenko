package com.gkuharenko.task1_2.service.impl;

import com.gkuharenko.task1_2.domain.Article;
import com.gkuharenko.task1_2.exception.NotFoundException;
import com.gkuharenko.task1_2.service.api.ClientService;
import com.gkuharenko.task1_2.util.ConverterUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

/**
 * Class that implements the interface
 *
 * @see ClientService
 */
public class HttpClientImpl implements ClientService {

    private HttpClient client;

    public HttpClientImpl() {
        client = HttpClientBuilder.create().build();
    }

    @Override
    public Article getArticle(String id) throws IOException {
        HttpGet httpGet = new HttpGet(URL + SLASH + id);
        HttpResponse response = client.execute(httpGet);
        Article article = ConverterUtils.toJavaObject(getResponse(response));
        return article;
    }

    @Override
    public Article postArticle() throws IOException {
        HttpPost httpPost = new HttpPost(URL);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("userId", "3"));
        nameValuePairs.add(new BasicNameValuePair("title", "foo"));
        nameValuePairs.add(new BasicNameValuePair("body", "bar"));
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        HttpResponse response = client.execute(httpPost);
        Article newArticle = ConverterUtils.toJavaObject(getResponse(response));
        return newArticle;
    }

    /**
     * The method returns response in the form of string. If the specified address is not found,
     * then throw NotFoundException
     *
     * @param httpResponse - response
     * @return - string response
     * @throws IOException
     */
    private String getResponse(HttpResponse httpResponse) throws IOException {
        if (httpResponse.getStatusLine().getStatusCode() == HTTP_NOT_FOUND) {
            throw new NotFoundException();
        }
        StringBuilder line = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                line.append(inputLine);
            }
        }
        return line.toString();
    }

}
