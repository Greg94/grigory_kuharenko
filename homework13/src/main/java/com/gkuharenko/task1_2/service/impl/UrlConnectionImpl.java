package com.gkuharenko.task1_2.service.impl;

import com.gkuharenko.task1_2.domain.Article;
import com.gkuharenko.task1_2.exception.NotFoundException;
import com.gkuharenko.task1_2.util.ConverterUtils;
import com.gkuharenko.task1_2.service.api.ClientService;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

/**
 * Class that implements the interface
 *
 * @see ClientService
 */
public class UrlConnectionImpl implements ClientService {

    private static final String REQUEST = "userId=3&title=foo&body=bar";

    public Article getArticle(String id) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) getConnection(URL + SLASH + id);
        Article article = ConverterUtils.toJavaObject(getResponse(connection));
        return article;
    }

    public Article postArticle() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) getConnection(URL);
        connection.setDoOutput(true);
        try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
            outputStream.writeBytes(REQUEST);
            outputStream.flush();
        }
        Article newArticle = ConverterUtils.toJavaObject(getResponse(connection));
        return newArticle;
    }

    private URLConnection getConnection(String url) throws IOException {
        URL jsonPlaceholder = new URL(url);
        return jsonPlaceholder.openConnection();
    }

    /**
     * The method returns response in the form of string. If the specified address is not found,
     * then throw NotFoundException
     *
     * @param connection - connection
     * @return - string response
     * @throws IOException
     */
    private String getResponse(HttpURLConnection connection) throws IOException {
        if (connection.getResponseCode() == HTTP_NOT_FOUND) {
            throw new NotFoundException();
        }
        StringBuilder line = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                line.append(inputLine);
            }
        }
        return line.toString();
    }

}
