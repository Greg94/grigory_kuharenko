package com.gkuharenko.task1_2.service.api;


import com.gkuharenko.task1_2.domain.Article;

import java.io.IOException;

public interface ClientService {

    String URL = "https://jsonplaceholder.typicode.com/posts";
    String SLASH = "/";

    /**
     * Method gets an article in json and returns an object.
     *
     * @param id - id
     * @return - object Article
     * @throws IOException
     */
    Article getArticle(String id) throws IOException;

    /**
     * Method creates a new article and passes it to json
     *
     * @return - object Article
     * @throws IOException
     */
    Article postArticle() throws IOException;

}
