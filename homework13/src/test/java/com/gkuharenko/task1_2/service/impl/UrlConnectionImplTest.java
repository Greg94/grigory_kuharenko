package com.gkuharenko.task1_2.service.impl;

import com.gkuharenko.task1_2.domain.Article;
import com.gkuharenko.task1_2.exception.NotFoundException;
import com.gkuharenko.task1_2.service.api.ClientService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class UrlConnectionImplTest {

    private ClientService clientService;

    @Before
    public void setUp() throws Exception {
        clientService = new UrlConnectionImpl();
    }

    @Test
    public void testGetArticle() throws IOException {
        Article articleExpected = new Article();
        articleExpected.setId(77);
        articleExpected.setMessage("modi ut in nulla repudiandae dolorum nostrum eos\naut consequatur omnis\nut incidunt est omnis iste et quam\nvoluptates sapiente aliquam asperiores nobis amet corrupti repudiandae provident");
        articleExpected.setTitle("necessitatibus quasi exercitationem odio");
        articleExpected.setUserId(8);
        Assert.assertEquals(articleExpected, clientService.getArticle("77"));
    }

    @Test(expected = NotFoundException.class)
    public void testGetArticleFails() throws IOException {
        clientService.getArticle("101");
    }

    @Test
    public void testPostArticle() throws IOException{
        Article articleExpected = new Article(101, 3, "foo", "bar");
        Assert.assertEquals(articleExpected,clientService.postArticle());
    }
}