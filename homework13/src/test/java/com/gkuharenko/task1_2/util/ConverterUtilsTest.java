package com.gkuharenko.task1_2.util;

import com.gkuharenko.task1_2.domain.Article;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;


public class ConverterUtilsTest {

    @Test
    public void toJavaObject() throws IOException {
        Article articleExpected = new Article();
        articleExpected.setId(77);
        articleExpected.setMessage("modi ut in nulla repudiandae dolorum nostrum eos\naut consequatur omnis\nut incidunt est omnis iste et quam\nvoluptates sapiente aliquam asperiores nobis amet corrupti repudiandae provident");
        articleExpected.setTitle("necessitatibus quasi exercitationem odio");
        articleExpected.setUserId(8);
        String response = "{\n" +
                "    \"userId\": 8,\n" +
                "    \"id\": 77,\n" +
                "    \"title\": \"necessitatibus quasi exercitationem odio\",\n" +
                "    \"body\": \"modi ut in nulla repudiandae dolorum nostrum eos\\naut consequatur omnis\\nut incidunt est omnis iste et quam\\nvoluptates sapiente aliquam asperiores nobis amet corrupti repudiandae provident\"\n" +
                "  }";

        Assert.assertEquals(articleExpected, ConverterUtils.toJavaObject(response));
    }

}