package com.gkuharenko.task1.servlet;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.service.ItemService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.gkuharenko.task1.servlet.ItemServlet.LOGIN_PARAMETER;

public class OrderServlet extends HttpServlet {

    private static final String ITEMS_PARAMETER = "listItems";
    private static final String NO_ITEMS_MSG = "No items selected.";
    private static final int TOP_OF_LISTING = 1;
    private ItemService itemService;

    @Override
    public void init() throws ServletException {
        itemService = new ItemService();
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] listItemsId = req.getParameterValues(ITEMS_PARAMETER);
        ServletContext servletContext = req.getServletContext();
        String login = String.valueOf(servletContext.getAttribute(LOGIN_PARAMETER));

        String body = "<html>"
                + "<body align=center>"
                + "<h1>Dear " + login + ", your order:" + "</h1>"
                + showTableResult(listItemsId)
                + "</body>"
                + "</html>";
        resp.getWriter().println(body);
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    /**
     * Displays a table with selected items
     *
     * @param itemsId array of selected id
     * @return {@link String}
     */
    private String showTableResult(String[] itemsId) {
        if (itemsId != null) {
            StringBuilder result = new StringBuilder();
            int count = TOP_OF_LISTING;
            for (Item item : itemService.doResultItems(itemsId)) {
                result.append("<p> <tr>");
                result.append("<td>").append(count++).append(") ").append("</td>");
                result.append("<td>").append(item.getName()).append(" ").append("</td>");
                result.append("<td>").append(item.getPrice()).append(" $").append("</td>");
                result.append("</tr> </p>");
            }
            double totalPrice = itemService.doCountPrice(itemsId);
            result.append("<p>Total: ").append(totalPrice).append("$").append("</p>");
            return result.toString();
        }
        return NO_ITEMS_MSG;
    }

}
