package com.gkuharenko.task1.servlet;


import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.repository.ItemRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class ItemServlet extends HttpServlet {

    static final String LOGIN_PARAMETER = "login";
    private static final String UNKNOWN_LOGIN = "anonymous";

    private ItemRepository itemRepository;

    @Override
    public void init() throws ServletException {
        itemRepository = new ItemRepository();
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = Optional.ofNullable(req.getParameter(LOGIN_PARAMETER)).orElse(UNKNOWN_LOGIN);
        ServletContext servletContext = req.getServletContext();
        servletContext.setAttribute(LOGIN_PARAMETER, login);
        String body = "<html>"
                + "<body align=center>"
                + "<h1>Hello, " + login + "!" + "</h1>"
                + "<form method='post' action=order>"
                + showListItems()
                + "<p><input type=submit value=Enter></p>"
                + "</form>"
                + "</body>"
                + "</html>";
        resp.getWriter().println(body);
    }

    /**
     * Adds a list of available items to html markup
     *
     * @return {@link String}
     */
    private String showListItems() {
        StringBuilder result = new StringBuilder();
        result.append("<p><select name='listItems' size='5' multiple required>");
        for (Item item : itemRepository.getDemoItems()) {
            result.append("<option value=")
                    .append(item.getId())
                    .append(">")
                    .append(item.getName())
                    .append("(")
                    .append(item.getPrice())
                    .append("$)</option>");
        }
        result.append("</select> </p>");
        return result.toString();
    }

}
