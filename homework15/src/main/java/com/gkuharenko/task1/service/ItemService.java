package com.gkuharenko.task1.service;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.repository.ItemRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ItemService {

    private ItemRepository itemRepository;

    public ItemService() {
        this.itemRepository = new ItemRepository();
    }

    /**
     * Returns a list of selected items
     *
     * @param itemsId array id
     * @return {@link List<Item>}
     */
    public List<Item> doResultItems(String[] itemsId) {
        return Arrays.stream(itemsId).map(this::findItem).collect(Collectors.toList());
    }

    /**
     * Considers the total value of items
     *
     * @param itemsId array id
     * @return {@link double}
     */
    public double doCountPrice(String[] itemsId) {
        return doResultItems(itemsId).stream().mapToDouble(Item::getPrice).sum();
    }

    /**
     * Search item from id
     *
     * @param id item id
     * @return {@link Item}
     */
    private Item findItem(String id) {
        return itemRepository.getDemoItems()
                .stream()
                .filter(item -> item.getId().equals(Long.parseLong(id)))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

}
