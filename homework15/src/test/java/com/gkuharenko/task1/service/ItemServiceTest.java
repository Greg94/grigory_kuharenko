package com.gkuharenko.task1.service;

import com.gkuharenko.task1.domain.Item;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ItemServiceTest {

    private ItemService itemService;

    @Before
    public void setUp() throws Exception {
        itemService = new ItemService();
    }

    @Test
    public void testDoResultItems() {
        String[] itemsId = {"1", "4"};
        List<Item> expected = new ArrayList<>();
        expected.add(new Item(1L, "Book", 5.5));
        expected.add(new Item(4L, "Laptop", 120.45));
        Assert.assertEquals(expected, itemService.doResultItems(itemsId));
    }

    @Test
    public void testDoCountPrice() {
        String[] itemsId = {"3", "4"};
        double expected = 220.45;
        Assert.assertEquals(expected, itemService.doCountPrice(itemsId), 0);
    }

}