package com.gkuharenko.task1_2.service.impl;

import com.gkuharenko.task1_2.service.api.ConnectionStrategy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class HttpClientImplTest {

    private ConnectionStrategy connectionStrategy;

    @Before
    public void setUp() throws Exception {
        connectionStrategy = new HttpClientImpl();
    }

    @Test
    public void testDoGet() throws IOException {
        String expected = "{  \"userId\": 1,  \"id\": 7,  \"title\": \"magnam facilis autem\",  \"body\": \"dolore placeat quibusdam ea quo vitae\\nmagni quis enim qui quis quo nemo aut saepe\\nquidem repellat excepturi ut quia\\nsunt ut sequi eos ea sed quas\"}";
        Assert.assertEquals(expected, connectionStrategy.doGet("7"));
    }

    @Test
    public void testDoPost() throws IOException {
        String expected = "{  \"userId\": \"3\",  \"title\": \"foo\",  \"body\": \"bar\",  \"id\": 101}";
        Assert.assertEquals(expected, connectionStrategy.doPost());
    }

    @Test
    public void testDoGetFails() throws IOException {
        String expected = "{}";
        Assert.assertEquals(expected, connectionStrategy.doGet("102"));
    }


}