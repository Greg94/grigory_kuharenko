package com.gkuharenko.task1_2.service;

import com.gkuharenko.task1_2.domain.Article;
import com.gkuharenko.task1_2.exception.ServerErrorException;
import com.gkuharenko.task1_2.service.api.ConnectionStrategy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ArticleServiceTest {

    @Mock
    private ConnectionStrategy connectionStrategy;
    @InjectMocks
    private ArticleService articleService;

    @Test
    public void testArticleServiceGet() throws IOException {
        //given
        String id = "1";
        Article expected = new Article(1, 1, "some title", "some msg");
        String response = "{\n" +
                "  \"userId\": 1,\n" +
                "  \"id\": 1,\n" +
                "  \"title\": \"some title\",\n" +
                "  \"body\": \"some msg\"\n" +
                "}";

        when(connectionStrategy.doGet(id)).thenReturn(response);

        //when
        Optional<Article> article = Optional.ofNullable(articleService.getArticle(id));

        //then
        Assert.assertTrue(article.isPresent());
        Assert.assertEquals(expected, article.get());

        verify(connectionStrategy).doGet(id);
    }

    @Test
    public void testArticleServicePost() throws IOException {
        //given
        Article expected = new Article(1, 1, "some title", "some msg");
        String response = "{\n" +
                "  \"userId\": 1,\n" +
                "  \"id\": 1,\n" +
                "  \"title\": \"some title\",\n" +
                "  \"body\": \"some msg\"\n" +
                "}";

        when(connectionStrategy.doPost()).thenReturn(response);

        //when
        Optional<Article> article = Optional.ofNullable(articleService.postArticle());

        //then
        Assert.assertTrue(article.isPresent());
        Assert.assertEquals(expected, article.get());

        verify(connectionStrategy).doPost();
    }

    @Test(expected = ServerErrorException.class)
    public void testArticleServiceGetConnectionFails_ServerErrorException() throws IOException {
        //given
        String id = "1";

        when(connectionStrategy.doGet(id)).thenThrow(IOException.class);

        //when
        articleService.getArticle(id);

        //then
        verify(connectionStrategy).doGet(id);
    }

    @Test(expected = ServerErrorException.class)
    public void testArticleServicePostConnectionFails_ServerErrorException() throws IOException {
        //given
        when(connectionStrategy.doPost()).thenThrow(IOException.class);

        //when
        articleService.postArticle();

        //then
        verify(connectionStrategy).doPost();
    }

}