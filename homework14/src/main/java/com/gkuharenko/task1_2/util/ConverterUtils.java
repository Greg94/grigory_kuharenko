package com.gkuharenko.task1_2.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkuharenko.task1_2.domain.Article;

import java.io.IOException;

public final class ConverterUtils {

    private ConverterUtils() {
    }

    /**
     * Method utility converts string response to java object Article.
     *
     * @param response - response
     * @return - java object Article
     * @throws IOException the io exception
     */
    public static Article toJavaObject(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response, Article.class);
    }

}
