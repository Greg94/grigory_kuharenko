package com.gkuharenko.task1_2.exception;


import java.io.IOException;

public class NotFoundException extends IOException {

    @Override
    public String getMessage() {
        return "Not found.";
    }

}
