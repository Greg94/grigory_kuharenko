package com.gkuharenko.task1_2.exception;

public class ServerErrorException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Server error exception";
    }
}
