package com.gkuharenko.task1_2.service.api;

import java.io.IOException;

public interface ConnectionStrategy {

    String URL = "https://jsonplaceholder.typicode.com/posts";
    String SLASH = "/";
    String NOT_FOUND = "enter valid id";

    /**
     * Sends a get request and returns a response as a string
     *
     * @param id - id article
     * @return - response as string
     * @throws IOException the io exception
     */
    String doGet(String id) throws IOException;

    /**
     * Sends a post request and returns a response as a string
     *
     * @return - response as string
     * @throws IOException the io exception
     */
    String doPost() throws IOException;

}
