package com.gkuharenko.task1_2.service.impl;

import com.gkuharenko.task1_2.exception.NotFoundException;
import com.gkuharenko.task1_2.service.api.ConnectionStrategy;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

/**
 * Class that implements the interface
 *
 * @see ConnectionStrategy
 */
public class HttpClientImpl implements ConnectionStrategy {

    private static final Logger LOG = Logger.getLogger(HttpClientImpl.class);

    private HttpClient client;

    public HttpClientImpl() {
        this.client = HttpClientBuilder.create().build();
    }

    @Override
    public String doGet(String id) throws IOException {
        HttpGet httpGet = new HttpGet(URL + SLASH + id);
        HttpResponse response = client.execute(httpGet);
        return getResponse(response);
    }

    @Override
    public String doPost() throws IOException {
        HttpPost httpPost = new HttpPost(URL);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("userId", "3"));
        nameValuePairs.add(new BasicNameValuePair("title", "foo"));
        nameValuePairs.add(new BasicNameValuePair("body", "bar"));
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        HttpResponse response = client.execute(httpPost);
        return getResponse(response);
    }

    /**
     * The method returns response in the form of string. If the specified address is not found,
     * then throw NotFoundException
     *
     * @param httpResponse - response
     * @return - string response
     * @throws IOException the io exception
     */
    private String getResponse(HttpResponse httpResponse) throws IOException {
        StringBuilder line = new StringBuilder();
        if (httpResponse.getStatusLine().getStatusCode() == HTTP_NOT_FOUND) {
            try {
                throw new NotFoundException();
            } catch (NotFoundException e) {
                LOG.error(NOT_FOUND);
            }
        }
        try (BufferedReader in = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                line.append(inputLine);
            }
        }
        return line.toString();
    }

}
