package com.gkuharenko.task1_2.service.impl;

import com.gkuharenko.task1_2.exception.NotFoundException;
import com.gkuharenko.task1_2.service.api.ConnectionStrategy;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

/**
 * Class that implements the interface
 *
 * @see ConnectionStrategy
 */
public class UrlConnectionImpl implements ConnectionStrategy {

    private static final Logger LOG = Logger.getLogger(UrlConnectionImpl.class);

    private static final String REQUEST = "userId=3&title=foo&body=bar";
    private static final String EMPTY_BODY = "{}";

    @Override
    public String doGet(String id) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) getConnection(URL + SLASH + id);
        return getResponse(connection);
    }

    @Override
    public String doPost() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) getConnection(URL);
        connection.setDoOutput(true);
        try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
            outputStream.writeBytes(REQUEST);
            outputStream.flush();
        }
        return getResponse(connection);
    }

    private URLConnection getConnection(String url) throws IOException {
        URL jsonPlaceholder = new URL(url);
        return jsonPlaceholder.openConnection();
    }

    /**
     * The method returns response in the form of string. If the specified address is not found,
     * then throw NotFoundException
     *
     * @param connection - connection
     * @return - string response
     * @throws IOException the io exception
     */
    private String getResponse(HttpURLConnection connection) throws IOException {
        StringBuilder line = new StringBuilder();
        if (connection.getResponseCode() == HTTP_NOT_FOUND) {
            try {
                throw new NotFoundException();
            } catch (NotFoundException e) {
                line.append(EMPTY_BODY);
                LOG.error(NOT_FOUND);
                return line.toString();
            }
        }
        try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                line.append(inputLine);
            }
        }
        return line.toString();
    }

}
