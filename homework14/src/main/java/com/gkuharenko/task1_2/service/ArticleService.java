package com.gkuharenko.task1_2.service;

import com.gkuharenko.task1_2.domain.Article;
import com.gkuharenko.task1_2.exception.ServerErrorException;
import com.gkuharenko.task1_2.service.api.ConnectionStrategy;
import com.gkuharenko.task1_2.util.ConverterUtils;
import org.apache.log4j.Logger;

import java.io.IOException;

public class ArticleService {

    private static final Logger LOG = Logger.getLogger(ArticleService.class);

    private static final String ARTICLE_GET_MSG = "Article[%s]: User [%s] Title [\"%s\"] Message [\"%s\"]\n";
    private static final String ARTICLE_POST_MSG = "Article[%s] has been created: User [%s] Title [\"%s\"] Message [\"%s\"]\n";
    private static final String CONNECTION_ERROR = "No connection";

    private ConnectionStrategy connectionStrategy;

    public ArticleService(ConnectionStrategy connectionStrategy) {
        this.connectionStrategy = connectionStrategy;
    }

    /**
     * Based on connectionStrategy returns a get request in the form of an object article.
     * If there are problems with the server "https://jsonplaceholder.typicode.com/posts" then throws serverErrorException
     *
     * @param id - id article
     * @return object article
     */
    public Article getArticle(String id) {
        Article article;
        try {
            article = ConverterUtils.toJavaObject(connectionStrategy.doGet(id));
            LOG.info(String.format(ARTICLE_GET_MSG, article.getId(), article.getUserId(), article.getTitle(), article.getMessage()));
        } catch (IOException e) {
            LOG.error(CONNECTION_ERROR);
            throw new ServerErrorException();
        }
        return article;
    }

    /**
     * Based on connectionstrategy returns a post request in the form of an object article.
     * If there are problems with the server "https://jsonplaceholder.typicode.com/posts" then throws serverErrorException
     *
     * @return object newArticle
     */
    public Article postArticle() {
        Article newArticle;
        try {
            newArticle = ConverterUtils.toJavaObject(connectionStrategy.doPost());
            LOG.info(String.format(ARTICLE_POST_MSG, newArticle.getId(), newArticle.getUserId(), newArticle.getTitle(), newArticle.getMessage()));
        } catch (IOException e) {
            LOG.error(CONNECTION_ERROR);
            throw new ServerErrorException();
        }
        return newArticle;
    }

}
