package com.gkuharenko.task1_2;

import com.gkuharenko.task1_2.service.ArticleService;
import com.gkuharenko.task1_2.service.impl.HttpClientImpl;
import com.gkuharenko.task1_2.service.impl.UrlConnectionImpl;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class TaskPresentation {

    private static final Logger LOG = Logger.getLogger(TaskPresentation.class);

    private static final String FIRST_ARGUMENT = "1";
    private static final String SECOND_ARGUMENT = "2";
    private static final String EXIT = "0";
    private static final String ARTICLE_ID = "article id: ";

    private ArticleService articleService;

    /**
     * Method launches the application
     */
    void startPresentation() {
        try (Scanner scanner = new Scanner(System.in)) {
            doChoiceClientService(scanner);
        }
    }

    /**
     * The method provides a choice of service.
     *
     * @param scanner - scanner
     */
    private void doChoiceClientService(Scanner scanner) {
        while (true) {
            showMenu();
            String input = scanner.next();
            if (input.equals(FIRST_ARGUMENT)) {
                articleService = new ArticleService(new UrlConnectionImpl());
                choiceMethod(scanner);
            } else if (input.equals(SECOND_ARGUMENT)) {
                articleService = new ArticleService(new HttpClientImpl());
                choiceMethod(scanner);
            } else if (input.equals(EXIT)) {
                break;
            }
        }
    }

    /**
     * Method provides get or post selection
     *
     * @param scanner - scanner
     */
    private void choiceMethod(Scanner scanner) {
        while (true) {
            showMethods();
            String input = scanner.next();
            if (input.equals(FIRST_ARGUMENT)) {
                doGet(scanner);
            } else if (input.equals(SECOND_ARGUMENT)) {
                doPost();
            } else if (input.equals(EXIT)) {
                break;
            }
        }
    }

    /**
     * Calls the get method
     *
     * @param scanner - scanner
     */
    private void doGet(Scanner scanner) {
        System.out.print(ARTICLE_ID);
        String id = scanner.next();
        articleService.getArticle(id);
    }

    /**
     * Calls the post method
     */
    private void doPost() {
        articleService.postArticle();
    }

    /**
     * Method prints menu
     */
    private void showMenu() {
        System.out.println("########MENU########");
        System.out.println("1 - Use URLConnection.");
        System.out.println("2 - Use HTTPClient.");
        System.out.println("0 - Exit from application.");
    }

    /**
     * Print method selection
     */
    private void showMethods() {
        System.out.println("**********Method selection**********");
        System.out.println("1 - GET");
        System.out.println("2 - POST");
        System.out.println("0 - Return to previous menu");
        System.out.println("************************************");
    }
}
