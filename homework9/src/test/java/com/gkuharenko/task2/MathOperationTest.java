package com.gkuharenko.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MathOperationTest {

    private MathOperation<String> stringMathOperation;
    private MathOperation<Integer> integerMathOperation;
    private Set<String> set1String;
    private Set<String> set2String;
    private Set<Integer> set1Integer;
    private Set<Integer> set2Integer;

    @Before
    public void setUp() throws Exception {
        set1String = new HashSet<>(Arrays.asList("A", "B"));
        set2String = new HashSet<>(Arrays.asList("B", "C"));
        set1Integer = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));
        set2Integer = new HashSet<>(Arrays.asList(4, 5, 6, 7, 8));
        stringMathOperation = new MathOperation<>();
        integerMathOperation = new MathOperation<>();
    }

    @Test(expected = NullPointerException.class)
    public void testUnionFails() {
        stringMathOperation.union(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testIntersectionFails() {
        stringMathOperation.intersection(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testMinusFails() {
        stringMathOperation.minus(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testDifferenceFails() {
        stringMathOperation.difference(null, null);
    }

    @Test
    public void testUnionString() {
        Set<Object> expected = new HashSet<>(Arrays.asList("A", "B", "C"));
        Assert.assertArrayEquals(expected.toArray(), stringMathOperation.union(set1String, set2String).toArray());
    }

    @Test
    public void testIntersectionString() {
        Set<Object> expected = new HashSet<>(Collections.singletonList("B"));
        Assert.assertArrayEquals(expected.toArray(), stringMathOperation.intersection(set1String, set2String).toArray());
    }

    @Test
    public void testMinusString() {
        Set<Object> expected = new HashSet<>(Collections.singletonList("A"));
        Assert.assertArrayEquals(expected.toArray(), stringMathOperation.minus(set1String, set2String).toArray());
    }

    @Test
    public void testDifferenceString() {
        Set<Object> expected = new HashSet<>(Arrays.asList("A", "C"));
        Assert.assertArrayEquals(expected.toArray(), stringMathOperation.difference(set1String, set2String).toArray());
    }

    @Test
    public void testUnionInteger() {
        Set<Object> expected = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
        Assert.assertArrayEquals(expected.toArray(), integerMathOperation.union(set1Integer, set2Integer).toArray());
    }

    @Test
    public void testIntersectionInteger() {
        Set<Object> expected = new HashSet<>(Arrays.asList(4, 5));
        Assert.assertArrayEquals(expected.toArray(), integerMathOperation.intersection(set1Integer, set2Integer).toArray());
    }

    @Test
    public void testMinusInteger() {
        Set<Object> expected = new HashSet<>(Arrays.asList(1, 2, 3));
        Assert.assertArrayEquals(expected.toArray(), integerMathOperation.minus(set1Integer, set2Integer).toArray());
    }

    @Test
    public void testDifferenceInteger() {
        Set<Object> expected = new HashSet<>(Arrays.asList(1, 2, 3, 6, 7, 8));
        Assert.assertArrayEquals(expected.toArray(), integerMathOperation.difference(set1Integer, set2Integer).toArray());
    }

}