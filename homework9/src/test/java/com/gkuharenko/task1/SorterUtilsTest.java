package com.gkuharenko.task1;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;


public class SorterUtilsTest {

    @Test
    public void testDoSort() {
        List<Integer> expected = Arrays.asList(30, 103, 14);
        Assert.assertArrayEquals(expected.toArray(), SorterUtils.doSort(new Integer[]{14, 30, 103}).toArray());
    }

    @Test(expected = NullPointerException.class)
    public void testDoSortWhenNull() {
        SorterUtils.doSort(null);
    }

}