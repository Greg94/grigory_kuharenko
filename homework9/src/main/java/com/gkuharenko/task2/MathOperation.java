package com.gkuharenko.task2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class that performs basic math
 * operations on sets: union, intersection, difference,
 * an exception.
 */
public class MathOperation<T> {

    /**
     * Method returns union between sets.
     *
     * @param set1 - set1
     * @param set2 - set2
     * @return - union set
     * @throws - NullPointerException when null
     */
    public Set<T> union(Set<T> set1, Set<T> set2) {
        return Optional.of(Stream.concat(set1.stream(), set2.stream()).collect(Collectors.toSet())).orElseThrow(NullPointerException::new);
    }

    /**
     * Method returns the intersection between the sets.
     *
     * @param set1 - set1
     * @param set2 - set2
     * @return - intersection set
     * @throws - NullPointerException when null
     */
    public Set<T> intersection(Set<T> set1, Set<T> set2) {
        return Optional.of(set1.stream().filter(set2::contains).collect(Collectors.toSet())).orElseThrow(NullPointerException::new);
    }

    /**
     * Method returns the difference between the sets.
     *
     * @param set1 - set1
     * @param set2 - set2
     * @return - minus set
     * @throws - NullPointerException when null
     */
    public Set<T> minus(Set<T> set1, Set<T> set2) {
        return Optional.of(set1.stream().filter(integer -> !set2.contains(integer)).collect(Collectors.toSet())).orElseThrow(NullPointerException::new);
    }

    /**
     * Method returns an exception between sets.
     *
     * @param set1 - set1
     * @param set2 - set2
     * @return - difference set
     * @throws - NullPointerException when null
     */
    public Set<T> difference(Set<T> set1, Set<T> set2) {
        Set<T> differenceSet = union(set1, set2);
        for (T t : intersection(set1, set2)) {
            differenceSet.remove(t);
        }
        return Optional.of(differenceSet).orElseThrow(NullPointerException::new);
    }

}
