package com.gkuharenko.task2;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Task2 {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(4, 5, 6, 7, 8));

        MathOperation<Integer> operationUtils = new MathOperation<>();

        System.out.println("Union: " + operationUtils.union(set1, set2));
        System.out.println("Intersection: " + operationUtils.intersection(set1, set2));
        System.out.println("Minus: " + operationUtils.minus(set1, set2));
        System.out.println("Difference: " + operationUtils.difference(set1, set2));

    }
}
