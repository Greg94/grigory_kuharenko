package com.gkuharenko.task1;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Utility class that sorts an array in order of sum of digits
 * its numbers.
 */
public final class SorterUtils {

    private SorterUtils() {
    }

    /**
     * The method sorts an integer array in the order of the sum of digits its numbers.
     *
     * @param mas - array
     * @return - integer list
     * @throws - NullPointerException if the method returns null
     */
    public static List<Integer> doSort(Integer[] mas) {
        return Optional.of(Arrays.stream(mas).sorted((o1, o2) -> {
                    Integer a = o1.toString().chars().map(Character::getNumericValue).sum();
                    Integer b = o2.toString().chars().map(Character::getNumericValue).sum();
                    return a.compareTo(b);
                }
        ).collect(Collectors.toList())).orElseThrow(NullPointerException::new);
    }
}
