package com.gkuharenko.task1.service;

import com.gkuharenko.task1.domain.Item;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ItemServiceTest {

    private ItemService itemService;

    @Before
    public void setUp() throws Exception {
        itemService = new ItemService();
    }

    @Test
    public void testDoCountPrice() {
        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item(1L, "Book", 5.5));
        itemList.add(new Item(4L, "Laptop", 120.45));
        double expected = 125.95;
        Assert.assertEquals(expected, itemService.doCountPrice(itemList), 0);
    }

    @Test
    public void testDoListResult() {

        //given
        HttpSession httpSession = Mockito.mock(HttpSession.class);
        List<Item> itemListTest = new ArrayList<>();
        itemListTest.add(new Item(1L, "Book", 5.5));
        itemListTest.add(new Item(4L, "Laptop", 120.45));

        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item(1L, "Book", 5.5));
        itemList.add(new Item(4L, "Laptop", 120.45));
        itemList.add(new Item(3L, "Coffee Machine", 100));

        //when
        Mockito.when(httpSession.getAttribute("result")).thenReturn(itemListTest);
        List<Item> result = itemService.doListResult(httpSession, "3");

        //then
        Assert.assertEquals(itemList, result);

    }
}