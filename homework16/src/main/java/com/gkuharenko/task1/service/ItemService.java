package com.gkuharenko.task1.service;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.repository.ItemRepository;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class ItemService {

    public static final String RESULT = "result";
    private static final int TOP_OF_LISTING = 1;

    private ItemRepository itemRepository;

    public ItemService() {
        this.itemRepository = new ItemRepository();
    }

    /**
     * Considers the total value of items
     *
     * @param itemList selected items
     * @return {@link List<Item>}
     */
    public double doCountPrice(List<Item> itemList) {
        return itemList.stream().mapToDouble(Item::getPrice).sum();
    }

    /**
     * Search item from id
     *
     * @param id item id
     * @return {@link Item}
     */
    private Item findItem(String id) {
        return itemRepository.getDemoItems()
                .stream()
                .filter(item -> item.getId().equals(Long.valueOf(id)))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

    /**
     * Adds selectable items to the sheet and saves them in the session
     *
     * @param httpSession {@link HttpSession}
     * @param id          item id
     * @return {@link List<Item>}
     */
    public List<Item> doListResult(HttpSession httpSession, String id) {
        List<Item> itemList = new ArrayList<>();
        if (httpSession.getAttribute(RESULT) != null) {
            itemList = (List<Item>) httpSession.getAttribute(RESULT);
        }
        if (id != null) {
            itemList.add(findItem(id));
            httpSession.setAttribute(RESULT, itemList);
        }
        return itemList;
    }

    /**
     * Display a table with selected items and their total value
     *
     * @param itemList selected items
     * @return {@link String}
     */
    public String showTableResult(List<Item> itemList) {
        StringBuilder result = new StringBuilder();
        int count = TOP_OF_LISTING;
        for (Item item : itemList) {
            result.append("<p> <tr>");
            result.append("<td>").append(count++).append(") ").append("</td>");
            result.append("<td>").append(item.getName()).append(" ").append("</td>");
            result.append("<td>").append(item.getPrice()).append(" $").append("</td>");
            result.append("</tr> </p>");
        }
        return result.toString();
    }

}
