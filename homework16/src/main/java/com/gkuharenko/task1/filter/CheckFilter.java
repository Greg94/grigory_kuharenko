package com.gkuharenko.task1.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CheckFilter implements Filter {

    private static final String CHECK_ON = "on";
    private static final String REDIRECT_PATH = "/error";
    public static final String CHECK_PARAMETER = "checkMe";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String checkMe = servletRequest.getParameter(CHECK_PARAMETER);
        HttpSession httpSession = ((HttpServletRequest) servletRequest).getSession();
        String isCheck = String.valueOf(httpSession.getAttribute(CHECK_PARAMETER));

        if (CHECK_ON.equals(checkMe) || CHECK_ON.equals(isCheck)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            resp.sendRedirect(REDIRECT_PATH);
        }
    }

}
