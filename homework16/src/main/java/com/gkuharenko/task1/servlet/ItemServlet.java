package com.gkuharenko.task1.servlet;


import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.repository.ItemRepository;
import com.gkuharenko.task1.service.ItemService;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

import static com.gkuharenko.task1.filter.CheckFilter.CHECK_PARAMETER;

public class ItemServlet extends HttpServlet {

    static final String LOGIN_PARAMETER = "login";
    private static final String ADD_PARAMETER = "addItem";

    private ItemRepository itemRepository;
    private ItemService itemService;

    @Override
    public void init() throws ServletException {
        itemRepository = new ItemRepository();
        itemService = new ItemService();
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String isCheck = req.getParameter(CHECK_PARAMETER);
        HttpSession httpSession = req.getSession();
        String login = req.getParameter(LOGIN_PARAMETER);
        String itemId = req.getParameter(ADD_PARAMETER);
        List<Item> result = itemService.doListResult(httpSession, itemId);
        if (login == null) {
            login = (String) httpSession.getAttribute(LOGIN_PARAMETER);
        } else {
            httpSession.setAttribute(LOGIN_PARAMETER, login);
            httpSession.setAttribute(CHECK_PARAMETER, isCheck);
        }
        String body = "<html>"
                + "<body align=center>"
                + "<h1>Hello, " + login + "!" + "</h1>"
                + "<form method='post' action=items>"
                + showListItems()
                + itemService.showTableResult(result)
                + "<p><input type=submit value=Add Item></p>"
                + "</form>"
                + "<form method='post' action=order>"
                + "<p><input type=submit value=Submit></p>"
                + "</form>"
                + "<p><a href='/logout'>Logout</a></p>"
                + "</body>"
                + "</html>";
        resp.getWriter().println(body);
    }

    /**
     * Adds a list of available items to html markup
     *
     * @return {@link String}
     */
    private String showListItems() {
        StringBuilder result = new StringBuilder();
        result.append("<p><select name='addItem' required>");
        for (Item item : itemRepository.getDemoItems()) {
            result.append("<option value=")
                    .append(item.getId())
                    .append(">")
                    .append(item.getName())
                    .append("(")
                    .append(item.getPrice())
                    .append("$)</option>");
        }
        result.append("</select> </p>");
        return result.toString();
    }

}
