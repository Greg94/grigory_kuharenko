package com.gkuharenko.task1.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ErrorServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body =
                "<html>"
                        + "<body align=center>"
                        + "<h1>Oops!</h1>"
                        + "<p>You should`t be here</p>"
                        + "<p>Please, agree with the terms of service first</p>"
                        + "<p><a href='/login'>Start page</a></p>"
                        + "</body>"
                        + "</html>";
        resp.getWriter().println(body);
    }

}
