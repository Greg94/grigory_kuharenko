package com.gkuharenko.task1.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body =
                "<html>"
                        + "<body align=center>"
                        + "<h1>Welcome to Online Shop</h1>"
                        + "<form method='post' action=items>"
                        + "<p><input type=text name='login' required placeholder='Enter your name'></p>"
                        + "<p><input type=checkbox name='checkMe'/>I agree with the terms of service.</p>"
                        + "<p><input type=submit value=Submit></p>"
                        + "</form>"
                        + "</body>"
                        + "</html>";
        resp.getWriter().println(body);
    }

}
