package com.gkuharenko.task1.servlet;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.service.ItemService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.gkuharenko.task1.service.ItemService.RESULT;
import static com.gkuharenko.task1.servlet.ItemServlet.LOGIN_PARAMETER;

public class OrderServlet extends HttpServlet {

    private static final String NO_ITEMS_MSG = "No items selected.";
    private ItemService itemService;

    @Override
    public void init() throws ServletException {
        itemService = new ItemService();
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession httpSession = req.getSession();
        String login = String.valueOf(httpSession.getAttribute(LOGIN_PARAMETER));
        List<Item> resultList = (List<Item>) httpSession.getAttribute(RESULT);
        String body = "<html>"
                + "<body align=center>"
                + "<h1>Dear " + login + ", your order:" + "</h1>"
                + showTotal(resultList)
                + "<p><a href='/logout'>Logout</a></p>"
                + "</body>"
                + "</html>";
        resp.getWriter().println(body);
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param req  the {@link HttpServletRequest}
     * @param resp the {@link HttpServletResponse}
     * @throws IOException the io exception
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    /**
     * Shows a list of goods and a conclusion on their total cost
     *
     * @param itemList selected items
     * @return {@link String}
     */
    private String showTotal(List<Item> itemList) {
        if (itemList != null) {
            String result = itemService.showTableResult(itemList);
            double totalPrice = itemService.doCountPrice(itemList);
            return result + "<p>Total: " + totalPrice + "$</p>";
        }
        return NO_ITEMS_MSG;
    }

}
