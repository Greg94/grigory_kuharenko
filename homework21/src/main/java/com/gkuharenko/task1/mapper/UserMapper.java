package com.gkuharenko.task1.mapper;

import com.gkuharenko.task1.domain.Role;
import com.gkuharenko.task1.domain.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserMapper implements RowMapper<User> {

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_LOGIN = "login";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_ROLE = "role";

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(COLUMN_ID));
        user.setLogin(resultSet.getString(COLUMN_LOGIN));
        user.setPassword(resultSet.getString(COLUMN_PASSWORD));
        user.setRole(convertToRole(resultSet));
        return user;
    }

    private Role convertToRole(ResultSet resultSet) throws SQLException {
        if (resultSet.getString(COLUMN_ROLE).equals(Role.ADMIN.toString())) {
            return Role.ADMIN;
        } else if (resultSet.getString(COLUMN_ROLE).equals(Role.USER.toString())) {
            return Role.USER;
        } else {
            return Role.ANONYMOUS;
        }
    }

}
