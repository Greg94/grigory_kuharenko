package com.gkuharenko.task1.mapper;

import com.gkuharenko.task1.domain.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;


@Component
public class OrderMapper implements RowMapper<Order> {

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_TOTAL_PRICE = "total_price";

    @Override
    public Order mapRow(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order();
        order.setId(resultSet.getLong(COLUMN_ID));
        order.setUserID(resultSet.getLong(COLUMN_USER_ID));
        order.setTotalPrice(resultSet.getBigDecimal(COLUMN_TOTAL_PRICE));
        return order;
    }
}
