package com.gkuharenko.task1.domain;

public enum Role {
    ADMIN,
    USER,
    ANONYMOUS
}
