package com.gkuharenko.task1.service.impl;

import com.gkuharenko.task1.domain.Role;
import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.exception.UserNotFoundException;
import com.gkuharenko.task1.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;


@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository repository;

    public CustomUserDetailsService(final UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(final String login) throws UserNotFoundException {

        final User user = repository
                .findUserByLogin(login)
                .orElseThrow(() -> new UserNotFoundException("User with login='" + login + "' not found"));

        String userLogin = user.getLogin();
        String userPassword = user.getPassword();
        Role userRole = user.getRole();

        return org.springframework.security.core.userdetails.User
                .withUsername(userLogin)
                .password(userPassword)
                .roles(userRole.toString())
                .build();
    }

    public String getPrincipal() {
        String userName;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

}
