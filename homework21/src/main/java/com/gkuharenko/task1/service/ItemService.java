package com.gkuharenko.task1.service;

import com.gkuharenko.task1.domain.Item;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface ItemService {

    /**
     * Get all items
     *
     * @return {@link List<Item>}
     */
    List<Item> getAllItems();

    /**
     * Get item by id
     *
     * @param id item id
     * @return {@link Item}
     */
    Item getItemById(String id);

    /**
     * Makes a temporary list of goods stored in the session
     *
     * @param httpSession http session
     * @param id          item id
     * @return {@link List<Item>}
     */
    List<Item> doTemporaryListItems(HttpSession httpSession, String id);

}
