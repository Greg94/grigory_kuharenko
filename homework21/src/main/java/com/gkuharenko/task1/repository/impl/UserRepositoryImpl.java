package com.gkuharenko.task1.repository.impl;

import com.gkuharenko.task1.domain.User;
import com.gkuharenko.task1.mapper.UserMapper;
import com.gkuharenko.task1.repository.UserRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final String SQL_SELECT_BY_LOGIN = "SELECT * FROM users AS u WHERE u.login= ?";

    private JdbcTemplate jdbcTemplate;
    private UserMapper userMapper;

    public UserRepositoryImpl(DataSource dataSource, UserMapper userMapper) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.userMapper = userMapper;
    }

    @Override
    public Optional<User> findUserByLogin(String login) {
        User user;
        try {
            user = jdbcTemplate.queryForObject(SQL_SELECT_BY_LOGIN, new Object[]{login}, userMapper);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
        return Optional.ofNullable(user);
    }

}
