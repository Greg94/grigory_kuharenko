package com.gkuharenko.task1.repository.impl;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.mapper.ItemMapper;
import com.gkuharenko.task1.repository.ItemRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;


@Repository
public class ItemRepositoryImpl implements ItemRepository {

    private static final String SQL_SELECT_ALL_ITEMS = "SELECT * FROM Items";
    private static final String SQL_FIND_ITEM_BY_ID = "SELECT * FROM Items AS i WHERE i.id= ?";

    private JdbcTemplate jdbcTemplate;
    private ItemMapper itemMapper;

    public ItemRepositoryImpl(DataSource dataSource, ItemMapper itemMapper) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.itemMapper = itemMapper;
    }

    @Override
    public List<Item> listItems() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ITEMS, itemMapper);
    }

    @Override
    public Optional<Item> findItemById(String id) {
        return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_FIND_ITEM_BY_ID, new Object[]{Long.parseLong(id)}, itemMapper));
    }

}
