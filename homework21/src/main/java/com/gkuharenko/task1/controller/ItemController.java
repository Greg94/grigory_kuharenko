package com.gkuharenko.task1.controller;

import com.gkuharenko.task1.domain.Item;
import com.gkuharenko.task1.service.ItemService;
import com.gkuharenko.task1.service.impl.CustomUserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

import static com.gkuharenko.task1.controller.ItemController.ONLINE_SHOP_PATH;
import static com.gkuharenko.task1.service.impl.ItemServiceImpl.RESULT;

@Controller
@RequestMapping(ONLINE_SHOP_PATH)
public class ItemController {

    static final String ONLINE_SHOP_PATH = "/online-shop";
    static final String USER_ATTRIBUTE = "user";
    private static final String ADD_PARAMETER = "addItem";
    private static final String LIST_ITEMS = "listItems";
    private static final String ITEMS_PATH = "/items";
    private static final String ITEMS_VIEW = "items";

    private ItemService itemService;
    private CustomUserDetailsService userDetailsService;

    public ItemController(ItemService itemService, CustomUserDetailsService userDetailsService) {
        this.itemService = itemService;
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, path = ITEMS_PATH)
    public String showItems(HttpServletRequest req, Model model) {
        HttpSession httpSession = req.getSession();
        String itemId = req.getParameter(ADD_PARAMETER);
        itemService.doTemporaryListItems(httpSession, itemId);
        if (req.getMethod().equals(RequestMethod.GET.toString())) {
            httpSession.removeAttribute(RESULT);
        }
        model.addAttribute(USER_ATTRIBUTE, userDetailsService.getPrincipal());
        return ITEMS_VIEW;
    }

    /**
     * Method used to populate the subjects list in view.
     *
     * @return {@link List<Item>}
     */
    @ModelAttribute(LIST_ITEMS)
    public List<Item> initializeItems() {
        return itemService.getAllItems();
    }

}
