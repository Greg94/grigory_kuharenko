package com.gkuharenko.task1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    private static final String LOGIN_PATH = "/online-shop";
    private static final String LOGIN_VIEW = "index";
    private static final String ERROR_PATH = "/error";
    private static final String ERROR_FLAG = "loginError";

    @GetMapping(LOGIN_PATH)
    public String loginPage() {
        return LOGIN_VIEW;
    }

    @GetMapping(ERROR_PATH)
    public String loginError(Model model) {
        model.addAttribute(ERROR_FLAG, true);
        return LOGIN_VIEW;
    }

}
