CREATE TABLE Users (id int NOT NULL AUTO_INCREMENT, login VARCHAR(255), password VARCHAR(255), role VARCHAR(255), PRIMARY KEY(ID));
CREATE TABLE Items (id int NOT NULL AUTO_INCREMENT, name VARCHAR(255), price double, PRIMARY KEY(ID));
CREATE TABLE Orders (id int NOT NULL AUTO_INCREMENT, user_id int, FOREIGN KEY(user_id) REFERENCES Users(id), total_price double);
CREATE TABLE Order_Items (order_id int, item_id int, FOREIGN KEY (order_id) REFERENCES Orders(id), FOREIGN KEY (item_id) REFERENCES Items(id));

INSERT INTO Users(login, password, role) VALUES ('admin', '$2a$04$z64lCcmodsQ8/o0Ls0RCluYhhWZZcz3CCwL4XdaHrFtoSCCH6t5lq','ADMIN');
INSERT INTO Users(login, password, role) VALUES ('user', '$2y$12$jYOqxcyDijtENCAtCO/aX.TumHlWIbzM.O3c2BQkw/KQQa0bEicAi','USER');

INSERT INTO Items(name, price) VALUES ('Book', 5.5);
INSERT INTO Items(name, price) VALUES ('Mobile Phone', 60.8);
INSERT INTO Items(name, price) VALUES ('Coffee Machine', 100);
INSERT INTO Items(name, price) VALUES ('Laptop', 120.45);
INSERT INTO Items(name, price) VALUES ('Cup', 3.2);

