# HOMEWORK 21 #
###### Launch of the project ######
1. Use to start maven plugin "jetty"
2. Execute command "jetty:run" 

###### Can execute ######
```
http://localhost:8080/online-shop      <== start page
http://localhost:8080/online-shop/items
http://localhost:8080/online-shop/order
```

USER_1 `login: admin || password: admin`
USER_2 `login: user || password: user`